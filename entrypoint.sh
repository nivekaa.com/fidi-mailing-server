#!/bin/bash

java ${MS_JAVA_OPTS} -jar /app.war \
  ${MS_SPRING_OPTS:-'--spring.config.location=/config/config.yml'} \
  --external.config.location=/config \
  -Dserver.port=8080 \
  -Dserver.address=0.0.0.0 \
  -Djava.security.egd=file:/dev/./urandom \
  -Dlogback.configurationFile=/config/logback.xml