package com.nivekaa.mailingserver.configs;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.nivekaa.mailingserver.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import springfox.documentation.PathProvider;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;


/**
 * @author nivekaa
 * Created 23/02/2020 at 13:47
 * Class com.nivekaa.mailingserver.configs.SwaggerConfig
 */

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    final
    Environment env;

    public SwaggerConfig(Environment env) {
        this.env = env;
    }

    @Bean
    public Docket produceApi() {
        ParameterBuilder aParameterBuilder = new ParameterBuilder();
        ParameterBuilder aParameterBuilder2 = new ParameterBuilder();
        aParameterBuilder.name(Util.HEADER_ENTERPRISE_ID_KEY)
                .parameterType("header")
                .modelRef(new ModelRef("string"))
                .defaultValue("1");

        aParameterBuilder2.name(Util.HEADER_USER_ID_KEY)
                .parameterType("header")
                .modelRef(new ModelRef("string"))
                .defaultValue("1");
        java.util.List<Parameter> aParameters = new ArrayList<>();
        aParameters.add(aParameterBuilder.build());
        aParameters.add(aParameterBuilder2.build());
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.nivekaa.mailingserver.rest.controllers"))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(aParameters);
    }

    // Describe your apis
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Miscroservice Mailing Management Rest APIs")
                .description("This page lists all the rest apis for Mailing Management App.")
                .version(env.getProperty("version"))
                .license("Apache 2.0")
                .contact((new Contact("Kevin Kemta","https://nivekaa.com","kevinlactiokemta@gmail.com")))
                .termsOfServiceUrl("https://nivekaa.con/terms")
                .build();
    }
    // Only select apis that matches the given Predicates.
    private Predicate<String> paths() {
        // Match all paths except /error
        return Predicates.and(
                PathSelectors.regex("mailing-server.*"),
                Predicates.not(PathSelectors.regex("/error.*"))
        );
    }

}
