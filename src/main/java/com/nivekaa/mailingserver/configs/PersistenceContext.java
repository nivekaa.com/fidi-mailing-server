package com.nivekaa.mailingserver.configs;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;

/**
 * @author dell
 * Created 11/02/2020 at 13:01
 * Class com.nivekaa.mailingserver.configs.PersistenceContext
 */

@Configuration
@EnableJpaRepositories(basePackages = "com.nivekaa.mailingserver.repositories")
@EnableTransactionManagement
public class PersistenceContext {
    @Bean
    JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setFailEarlyOnGlobalRollbackOnly(false);
        transactionManager.setGlobalRollbackOnParticipationFailure(false);
        // transactionManager.setFailEarlyOnGlobalRollbackOnly(false);
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }


    @Bean
    public HibernateTransactionManager transactionManager(SessionFactory s) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(s);
        return txManager;
    }
}
