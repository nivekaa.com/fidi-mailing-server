package com.nivekaa.mailingserver.configs;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author nivekaa
 * Created 30/03/2020 at 23:57
 * Class com.nivekaa.mailingserver.configs.MailingConfiguration
 */

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "mailing")
public class ConfigMailingConfiguration {
    private long maxSizeFilesUpload;
    private Integer maxAttachments;
}
