package com.nivekaa.mailingserver.configs;

import com.nivekaa.mailingserver.Util;
import com.nivekaa.mailingserver.rest.error.ApiException;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

/**
 * @author nivekaa
 * Created 30/03/2020 at 01:42
 * Class com.nivekaa.mailingserver.configs.RequestInterceptor
 */

@Component
//@Order(-2)
public class RequestInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if (request.getRequestURI().contains("swagger")
                || request.getRequestURI().contains("/webjars/"))
            return true;


        if (Objects.isNull(request.getHeader(Util.HEADER_ENTERPRISE_ID_KEY))){
            throw new ApiException.BadArgumentsException("Headers missing X-ENTERPRISE-ID");
        }
        if (Objects.isNull(request.getHeader(Util.HEADER_USER_ID_KEY))){
            throw new ApiException.BadArgumentsException("Headers missing X-USER-ID");
        }
        return true;
    }
}
