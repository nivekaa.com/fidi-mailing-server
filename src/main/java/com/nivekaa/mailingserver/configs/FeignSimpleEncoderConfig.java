package com.nivekaa.mailingserver.configs;

import feign.codec.Encoder;
import feign.form.FormEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author dell
 * Created 13/02/2020 at 00:17
 * Class com.nivekaa.mailingserver.configs.FeignSimpleEncoderConfig
 */

@Configuration
public class FeignSimpleEncoderConfig {
    @Bean
    public Encoder encoder() {
        return new FormEncoder();
    }
}
