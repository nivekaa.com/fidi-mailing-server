package com.nivekaa.mailingserver.configs;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author dell
 * Created 10/02/2020 at 23:52
 * Class com.nivekaa.mailingserver.configs.MailProperties
 */
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "spring.mail", ignoreInvalidFields = true)
public class MailProperties {
    private String host;
    private String username;
    private String password;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
