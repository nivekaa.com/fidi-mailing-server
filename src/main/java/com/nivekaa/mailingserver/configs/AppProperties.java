package com.nivekaa.mailingserver.configs;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author kevin
 * Created 15/10/2020 at 16:23
 * Class com.nivekaa.mailingserver.configs.AppProperties
 */
@Configuration
@ConfigurationProperties(prefix = "app")
@Data
public class AppProperties {
    private String baseUrl;
}
