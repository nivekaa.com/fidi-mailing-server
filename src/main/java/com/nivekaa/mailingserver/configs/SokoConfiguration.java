package com.nivekaa.mailingserver.configs;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author nivekaa
 * Created 29/03/2020 at 17:49
 * Class com.nivekaa.mailingserver.configs.SokoConfiguration
 */

@Configuration
@ConfigurationProperties(prefix = "soko")
public class SokoConfiguration {
    @Getter
    @Setter
    private String apiKey;
    @Getter
    @Setter
    private String appName;
}
