package com.nivekaa.mailingserver.configs;

import com.nivekaa.mailingserver.model.Template;
import com.nivekaa.mailingserver.model.TemplateField;
import com.nivekaa.mailingserver.repositories.TemplateFieldRepository;
import com.nivekaa.mailingserver.repositories.TemplateRepository;
import com.nivekaa.mailingserver.rest.util.FilesUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;

/**
 * @author kevin
 * Created 13/10/2020 at 21:56
 * Class com.nivekaa.mailingserver.configs.DbInitializer
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class DbInitializer implements ApplicationListener<ContextRefreshedEvent> {
    private final InitDbProperties dbProperties;
    // private final AppProperties appProperties;
    private final TemplateRepository templateRepository;
    private final TemplateFieldRepository templateFieldRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        log.info("Starting Data initi<<<alization ...");
        log.info("Inserting template");
        dbProperties.getTemplates()
                .forEach(template -> {
                    Set<TemplateField> fields = template.getFields();
                    template.setFields(null);
                    Template saved = saveTemplateIfNotExist(template);
                    log.info("Inserting field for template {}", template.getCode());
                    fields.forEach(field -> {
                        field.setTemplate(Template.builder().id(saved.getId()).build());
                        field.setEntrepriseId(saved.getEntrepriseId());
                        saveTemplFieldIfNotExist(field);
                    });
                    log.info("Template #{} is been completely saved with these fields({})", saved.getCode(), fields.size());
                });
    }

    @Transactional
    public Template saveTemplateIfNotExist(Template template) {
        Optional<Template> optionalTemplate = templateRepository.findByCode(template.getCode());
        return optionalTemplate.orElseGet(() -> templateRepository.save(template));
    }

    @Transactional
    public TemplateField saveTemplFieldIfNotExist(TemplateField field) {
        Optional<TemplateField> optionalField = templateFieldRepository.findByNameAndTemplate_Id(field.getName(), field.getTemplate().getId());
        return optionalField.orElseGet(() -> templateFieldRepository.save(field));
    }
}
