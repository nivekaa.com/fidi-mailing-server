package com.nivekaa.mailingserver.configs;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.tags.Tag;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author nivekaa
 * Created 23/02/2020 at 13:47
 * Class com.nivekaa.mailingserver.configs.SwaggerConfig
 */

@Configuration
public class OpenApiConfig {
    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .info(new Info()
                        .title("Miscroservice Mailing Management Rest APIs")
                        .version("1.0.0")
                        .license(new License().name("Apache 2.0"))
                        .contact(new Contact().email("contact@nivekaa.com").name("NIVEKAA.COM").url("https://nivekaa.com"))
                        .termsOfService("")
                )
                .addTagsItem(new Tag().name("mytag"));
    }

}
