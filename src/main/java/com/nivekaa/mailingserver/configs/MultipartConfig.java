package com.nivekaa.mailingserver.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

/**
 * @author nivekaa
 * Created 23/02/2020 at 15:41
 * Class com.nivekaa.mailingserver.configs.MultipartConfig
 */

@Configuration
public class MultipartConfig {
    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        //multipartResolver.setMaxUploadSize(100000);

        return multipartResolver;
    }

    @Bean
    public StandardServletMultipartResolver multipartResolver2() {
        return new StandardServletMultipartResolver();
    }
}
