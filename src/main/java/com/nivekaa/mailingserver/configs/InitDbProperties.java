package com.nivekaa.mailingserver.configs;

import com.nivekaa.mailingserver.model.Template;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author kevin
 * Created 13/10/2020 at 21:37
 * Class com.nivekaa.mailingserver.configs.InitDbProperties
 */

@Component
@ConfigurationProperties
@PropertySource(value = "classpath:/initdata.yml", factory =  YamlPropertyLoaderFactory.class)
@ToString
@Setter @Getter
public class InitDbProperties {
    // @Value("${templates}")
    private List<Template> templates;
}
