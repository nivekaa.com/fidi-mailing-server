package com.nivekaa.mailingserver.jobs;

import com.nivekaa.mailingserver.model.FileStore;
import com.nivekaa.mailingserver.model.Mail;
import com.nivekaa.mailingserver.rest.util.FilesUtil;
import com.nivekaa.mailingserver.service.MailEntityService;
import com.nivekaa.mailingserver.service.dto.MailDTO;
import com.nivekaa.mailingserver.service.impl.MailServiceImpl;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * @author nivekaa
 * Created 23/02/2020 at 19:03
 * Class com.nivekaa.mailingserver.jobs.ScheduleEmailJob
 */

@Component
public class ScheduleEmailJob extends QuartzJobBean {
    private static final Logger logger = LoggerFactory.getLogger(ScheduleEmailJob.class);
    private final MailServiceImpl mailService;
    private final MailEntityService mailEntityService;

    public ScheduleEmailJob(MailServiceImpl mailService, MailEntityService mailEntityService) {
        this.mailService = mailService;
        this.mailEntityService = mailEntityService;
    }

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try{
            logger.info("Executing Job with key {}", jobExecutionContext.getJobDetail().getKey());
            JobKey key = jobExecutionContext.getJobDetail().getKey();
            JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
            Long id = jobDataMap.getLong("id");
            Long entrepriseId = jobDataMap.getLong("entrepriseId");
            MailDTO mailDTO = mailEntityService.getByEntreprise(id,entrepriseId);
            File[] files = getFile(mailDTO);
            Mail mail = new Mail();
            mail.setFrom(mailDTO.getFrom());
            mail.setSubject(mailDTO.getSubject());
            mail.setEntrepriseId(mailDTO.getEntrepriseId());
            mail.setTo(mailDTO.getTo());
            mail.setCc(mailDTO.getCc());
            mail.setBcc(mailDTO.getBcc());
            mail.setContent(mailDTO.getContent());
            logger.info("---------------Start job "+ String.format("%s[%s]", key.getGroup(), key.getName()) +"----------------");
            mailService.simpleWithAttachments(mail,true,files);
            logger.info("---------------End job "+ String.format("%s[%s]", key.getGroup(), key.getName()) +"----------------");
        } catch (Exception e) {
            logger.error("JobError: {}", e.getMessage());
        }
    }

    private File[] getFile(MailDTO mailDTO){
        File[] files;
        if (mailDTO.getFiles()!= null && !mailDTO.getFiles().isEmpty()){
            files = new File[mailDTO.getFiles().size()];
            int i=0;
            for (FileStore filestore: mailDTO.getFiles()) {
                files[i] = new File(FilesUtil.path+filestore.getFilename());
                i++;
            }
        }else
            files = new File[0];
        return files;
    }
}
