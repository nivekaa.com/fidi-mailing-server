package com.nivekaa.mailingserver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.format.DateTimeFormatter;

/**
 * @author nivekaa
 * Created 29/03/2020 at 19:03
 * Class com.nivekaa.mailingserver.Util
 */

public class Util {
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final String HEADER_USER_ID_KEY = "X-USER-ID";
    public static final String _CDR_OPERATIONS = "LOG-OPERATIONS";
    public static final String HEADER_ENTERPRISE_ID_KEY = "X-ENTERPRISE-ID";

    public static String objToJson(Object o) {
        if (o == null)
            return "{}";
        if (o instanceof String)
            return String.valueOf(o);
        if (o instanceof Integer)
            return String.valueOf(o);
        if (o instanceof Long)
            return String.valueOf(o);
        if (o instanceof Double)
            return String.valueOf(o);
        if (o instanceof Boolean)
            return String.valueOf(o);
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "{}";
        }
    }
}
