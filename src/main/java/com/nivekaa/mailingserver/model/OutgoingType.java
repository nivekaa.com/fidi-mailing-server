package com.nivekaa.mailingserver.model;

import lombok.Getter;

/**
 * @author nivekaa
 * Created 29/03/2020 at 17:01
 * Class com.nivekaa.mailingserver.model.OutgoingType
 */

public enum OutgoingType {
    SMTP("smtp"),
    SES("Ses"),// Amazon simple Email Service
    MANDRILL("mandrill"),
    JAVAMAILER("javamailer");

    @Getter
    private String type;
    OutgoingType(String type){
        this.type = type;
    }
}
