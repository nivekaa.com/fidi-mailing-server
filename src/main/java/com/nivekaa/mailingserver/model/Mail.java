package com.nivekaa.mailingserver.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import feign.form.FormProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * @author dell
 * Created 10/02/2020 at 23:50
 * Class com.nivekaa.mailingserver.model.Mail
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(name = "log_mail")
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class Mail extends Base implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "mail_from")
    @JsonProperty(value = "from", required = true)
    private String from;

    @Column(name = "mail_to")
    @JsonProperty(value = "to", required = true)
    private String to;

    @Column(name = "mail_cc")
    @JsonProperty(value = "cc", required = false)
    private String cc;

    @Column(name = "mail_bcc")
    @JsonProperty(value = "bcc", required = false)
    private String bcc;

    @Column(name = "subject")
    private String subject;

    @Column(name = "no_footer", columnDefinition = " boolean default false")
    private boolean noFooter;

    @Column(name = "no_header", columnDefinition = " boolean default false")
    private boolean noHeader;

    @FormProperty("content")
    @Column(name = "content", columnDefinition = " longtext null")
    private String content;

    @Column(name = "lang_key")
    transient private String langKey;

    @OneToMany(mappedBy = "mail", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    @Transient
    transient private Set<FileStore> files;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "local_date_time")
    private String dateTime;

    @NotNull
    @Column(name = "time_zone")
    private String timeZone;

    @NotNull
    @Column(name = "entreprise_id")
    private Long entrepriseId;

    public Mail(String from, String to, String subject, String content) {
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mail mail = (Mail) o;
        return id.equals(mail.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
