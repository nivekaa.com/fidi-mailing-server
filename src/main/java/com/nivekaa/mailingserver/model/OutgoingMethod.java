package com.nivekaa.mailingserver.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author nivekaa
 * Created 29/03/2020 at 16:17
 * Class com.nivekaa.mailingserver.model.OutgoingMethod
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "outgoing_mail_method")
@Builder
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OutgoingMethod extends Base implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "entreprise_id")
    private Long entrepriseId;

    @Column(name = "method")
    private String method;
}
