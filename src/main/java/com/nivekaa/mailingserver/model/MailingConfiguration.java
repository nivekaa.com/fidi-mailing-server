package com.nivekaa.mailingserver.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nivekaa.mailingserver.service.dto.MConfigDTO;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author nivekaa
 * Created 29/03/2020 at 16:10
 * Class com.nivekaa.mailingserver.model.MailingConfiguration
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "mail_configuration")
@Builder
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MailingConfiguration extends Base implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "entreprise_id")
    private Long entrepriseId;

    @Column(name = "from_mail", nullable = false)
    private String fromEmail;

    @Column(name = "from_name", nullable = false)
    private String fromName;

    @Column(name = "footer_mail", columnDefinition = " LONGTEXT NULL DEFAULT NULL")
    private String footerContent;

    @Column(name = "header_mail", columnDefinition = " LONGTEXT NULL DEFAULT NULL ")
    private String headerContent;

    @Column(name = "outgoing_mail_method", columnDefinition = " VARCHAR(32) NULL DEFAULT 'smtp' ")
    private String outgoingMethod;


    public MConfigDTO toDto(){
        return MConfigDTO.builder()
                .id(getId())
                .entrepriseId(getEntrepriseId())
                .footerContent(getFooterContent())
                .headerContent(getHeaderContent())
                .fromEmail(getFromEmail())
                .fromName(getFromName())
                .outgoingMethod(getOutgoingMethod())
                .build();
    }

    public boolean exist(){
        return false;
    }
}
