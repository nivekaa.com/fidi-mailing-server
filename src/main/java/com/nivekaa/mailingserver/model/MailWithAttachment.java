package com.nivekaa.mailingserver.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRawValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.Map;

/**
 * @author nivekaa
 * Created 23/02/2020 at 15:44
 * Class com.nivekaa.mailingserver.model.MailWithAttachment
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "mail", "files", "data" })
public class MailWithAttachment implements Serializable {
    @JsonProperty("mail")
    private Mail mail;
    @JsonProperty(value = "data", required = true)
    @JsonRawValue
    private Map<String, Object> data;
    @JsonProperty(value = "files", required = true)
    private MultipartFile[] files;
}
