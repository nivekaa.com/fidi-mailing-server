package com.nivekaa.mailingserver.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author kevin
 * Created 13/10/2020 at 02:15
 * Class com.nivekaa.mailingserver.model.TempleteField
 */

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Builder
@Table(name = "mail_template_field")
public class TemplateField  extends Base{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @Column(name = "entreprise_id")
    private Long entrepriseId;
    @NotNull
    @Column(name = "field_name", length = 64, nullable = false)
    private String name;
    @Column(name = "field_description", columnDefinition = "text default null")
    private String description;
    @Column(name = "field_default_value", columnDefinition = "text default null")
    private String defaultValue;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "template_id")
    @ToString.Exclude
    private Template template;
}
