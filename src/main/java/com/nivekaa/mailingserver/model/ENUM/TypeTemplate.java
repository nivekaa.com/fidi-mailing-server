package com.nivekaa.mailingserver.model.ENUM;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author kevin
 * Created 15/10/2020 at 15:46
 * Class com.nivekaa.mailingserver.model.ENUM.TypeTemplate
 */

@AllArgsConstructor
public enum TypeTemplate {
    TEXT("text"), HTML("html");
    @Getter
    private String name;

}
