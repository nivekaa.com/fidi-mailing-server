package com.nivekaa.mailingserver.model;

import lombok.*;

/**
 * @author nivekaa
 * Created 23/02/2020 at 19:47
 * Class com.nivekaa.mailingserver.model.ScheduleEmailResponse
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
public class ScheduleEmailResponse{

    private boolean success;
    private String jobId;
    private String jobGroup;
    private String message;

    public ScheduleEmailResponse(boolean success, String message) {
        this.success = success;
        this.message = message;
    }
}
