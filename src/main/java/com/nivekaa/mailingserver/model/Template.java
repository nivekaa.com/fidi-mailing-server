package com.nivekaa.mailingserver.model;

import com.nivekaa.mailingserver.model.ENUM.TypeTemplate;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * @author kevin
 * Created 10/10/2020 at 01:34
 * Class com.nivekaa.mailingserver.model.Template
 */

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "mail_template")
public class Template extends Base{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "entreprise_id")
    private Long entrepriseId;

    @Column(name = "content_syntaxed", nullable = false, columnDefinition = "longtext not null")
    private String content;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "data_model",columnDefinition = "varchar(128) default 'java.util.Map'")
    private String dataModel;

    @Column(name = "type_template",columnDefinition = "enum('HTML','TEXT') default 'TEXT'")
    @Enumerated(EnumType.STRING)
    private TypeTemplate type;

    @OneToMany(mappedBy = "template", fetch = FetchType.EAGER,
            cascade = {CascadeType.REMOVE}, orphanRemoval = true)
    private Set<TemplateField> fields;

    public void addField(TemplateField field) {
        this.fields.add(field);
        field.setTemplate(this);
    }

    public void removeField(TemplateField field) {
        field.setTemplate(null);
        this.fields.remove(field);
    }
}
