package com.nivekaa.mailingserver.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author nivekaa
 * Created 29/03/2020 at 17:22
 * Class com.nivekaa.mailingserver.model.OutgoingMethodParams
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "outgoing_mail_method_param")
public class OutgoingMethodParams extends Base implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "entreprise_id")
    private Long entrepriseId;

    @NotNull
    @Column(name = "method_id")
    private Long methodId;

    @Column(name = "`key`")
    private String key;

    @Column(name = "value")
    private String value;
}
