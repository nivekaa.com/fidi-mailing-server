package com.nivekaa.mailingserver.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author nivekaa
 * Created 23/02/2020 at 19:09
 * Class com.nivekaa.mailingserver.model.FileStore
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "attachment_file_store")
@Builder
public class FileStore extends Base implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "location", nullable = false)
    private String location;
    @Column(name = "filename", nullable = false)
    private String filename;
    @Column(name = "file_type", nullable = true)
    private String fileType;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "log_mail_id", nullable = false)
    private Mail mail;

    @NotNull
    @Column(name = "entreprise_id")
    private Long entrepriseId;
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileStore f = (FileStore) o;
        return id.equals(f.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "FileStore{" +
                "id=" + id +
                ", location='" + location + '\'' +
                ", filename='" + filename + '\'' +
                ", fileType='" + fileType + '\'' +
                ", mail=" + mail +
                '}';
    }
}
