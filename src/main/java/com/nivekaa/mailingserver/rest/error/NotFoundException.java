package com.nivekaa.mailingserver.rest.error;

/**
 * @author kevin
 * Created 10/10/2020 at 03:15
 * Class com.nivekaa.mailingserver.rest.error.NotFoundException
 */

public class NotFoundException extends Exception {
    public NotFoundException() {
        super("Element not found");
    }

    public NotFoundException(Long id) {
        super(String.format("The element with ID equal %d not found", id));
    }

    public NotFoundException(String message) {
        super(message);
    }
}
