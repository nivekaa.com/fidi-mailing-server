package com.nivekaa.mailingserver.rest.error;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * @author nivekaa
 * Created 30/03/2020 at 00:18
 * Class com.nivekaa.mailingserver.rest.error.ApiError
 */

@Getter @Setter
public class ApiError {
    // @JsonIgnore
    private HttpStatus status;
    private String message;
    @JsonIgnore
    private String trace;
    @JsonIgnore
    private String debugMessage;
    private int statusCode;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;
    private List<ApiSubError> subErrors;

    public ApiError() {
        this.timestamp = LocalDateTime.now();
    }

    public ApiError(HttpStatus status) {
        this();
        this.status = status;
        this.statusCode = status.value();
    }

    public ApiError(HttpStatus status, Throwable ex) {
        this(status);
        this.message = "Unexpected error";
        this.debugMessage = ex.getLocalizedMessage();
        this.trace = Arrays.toString(ex.getStackTrace());
        ex.printStackTrace();
    }

    public ApiError(HttpStatus status, String message, Throwable ex) {
        this(status, ex);
        this.message = message;
    }
}
