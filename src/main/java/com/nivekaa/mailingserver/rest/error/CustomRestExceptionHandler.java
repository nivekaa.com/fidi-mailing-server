package com.nivekaa.mailingserver.rest.error;

import com.nivekaa.mailingserver.rest.util.CdrHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.nivekaa.mailingserver.rest.util.CdrHelper._CDR_FOR_ERROR;

/**
 * @author nivekaa
 * Created 30/03/2020 at 00:19
 * Class com.nivekaa.mailingserver.rest.error.CustomRestExceptionHandler
 */

@ControllerAdvice
@Profile({"dev","prod"})
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(_CDR_FOR_ERROR);

    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        String sub = apiError.getSubErrors() == null ? "NULL" : apiError.getSubErrors()
                .stream()
                .map(apiSubError -> {
                    ApiValidationError err = (ApiValidationError) apiSubError;
                    return String.format("FIELD=%s,OBJECT=%s,MESSAGE=%s,REJECTED=%s", err.getField(), err.getObject(), err.getMessage(), err.getRejectedValue());
                }).collect(Collectors.joining(";"));
        log.error(CdrHelper.formatCdrLog(String.valueOf(apiError.getStatusCode()), apiError.getMessage(),sub, apiError.getTrace()));
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String error = ex.getParameterName() + " parameter is missing";
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, error, ex);
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler({ MethodArgumentTypeMismatchException.class })
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex, WebRequest request) {
        String error = ex.getName() + " should be of type " + ex.getRequiredType().getName();
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, error, ex);
        return buildResponseEntity(apiError);
    }


    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> customHandleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
        String msg = "Constraint violation";
        ApiError error = new ApiError(HttpStatus.BAD_REQUEST, msg, ex);
        List<ApiSubError> subErrors = ex.getConstraintViolations()
                .stream()
                .map(cv -> {
                    ApiValidationError err = new ApiValidationError();
                    err.setObject(String.format("%s: value '%s' %s", cv.getPropertyPath(), cv.getInvalidValue(), cv.getMessage()));
                    err.setField(cv.getPropertyPath().toString());
                    err.setRejectedValue(cv.getInvalidValue());
                    err.setMessage(cv.getMessage());
                    return err;
                }).collect(Collectors.toList());
        error.setSubErrors(subErrors);
        return buildResponseEntity(error);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String error = "Malformed JSON request";
        return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, error, ex));
    }


    @ExceptionHandler({ RuntimeException.class })
    public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
        String message = "Internal error. Contact system administrator for more details about this problem";
        ApiError error = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, message, ex);
        return buildResponseEntity(error);
    }


    @ExceptionHandler({ NotFoundException.class })
    public ResponseEntity<Object> handleElementNotFound(NotFoundException ex, WebRequest request) {
        ApiError error = new ApiError(HttpStatus.NOT_FOUND, ex.getMessage(), ex);
        return buildResponseEntity(error);
    }



    // 400

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        List<ApiSubError> subErrors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(fieldError -> {
                    ApiValidationError error = new ApiValidationError();
                    error.setField(fieldError.getField());
                    error.setRejectedValue(fieldError.getRejectedValue());
                    error.setObject(fieldError.getObjectName());
                    error.setMessage(fieldError.getDefaultMessage());
                    return error;
                }).collect(Collectors.toList());

        List<ApiSubError> subErrors2 = ex.getBindingResult()
                .getGlobalErrors()
                .stream()
                .map(fieldError -> {
                    ApiValidationError error = new ApiValidationError();
                    // error.setField(fieldError.);
                    // error.setRejectedValue(fieldError.);
                    error.setObject(fieldError.getObjectName());
                    error.setMessage(fieldError.getDefaultMessage());
                    return error;
                }).collect(Collectors.toList());
        subErrors.addAll(subErrors2);
        final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, "Validation failed for argument", ex);
        apiError.setSubErrors(subErrors);
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(final BindException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        List<ApiSubError> subErrors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(fieldError -> {
                    ApiValidationError error = new ApiValidationError();
                    error.setField(fieldError.getField());
                    error.setRejectedValue(fieldError.getRejectedValue());
                    error.setObject(fieldError.getObjectName());
                    error.setMessage(fieldError.getDefaultMessage());
                    return error;
                }).collect(Collectors.toList());

        List<ApiSubError> subErrors2 = ex.getBindingResult()
                .getGlobalErrors()
                .stream()
                .map(fieldError -> {
                    ApiValidationError error = new ApiValidationError();
                    error.setObject(fieldError.getObjectName());
                    error.setMessage(fieldError.getDefaultMessage());
                    return error;
                }).collect(Collectors.toList());
        subErrors.addAll(subErrors2);
        final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, "", ex);
        apiError.setSubErrors(subErrors);
        return buildResponseEntity(apiError);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(final TypeMismatchException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        logger.info(ex.getClass().getName());
        //
        final String error = ex.getValue() + " value for " + ex.getPropertyName() + " should be of type " + ex.getRequiredType();

        final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, error, ex);
        return buildResponseEntity(apiError);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestPart(final MissingServletRequestPartException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        logger.info(ex.getClass().getName());
        //
        final String error = ex.getRequestPartName() + " part is missing";
        final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, error, ex);
        return buildResponseEntity(apiError);
    }

    // 404

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(final NoHandlerFoundException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        logger.info(ex.getClass().getName());
        //
        final String error = "No handler found for " + ex.getHttpMethod() + " " + ex.getRequestURL();

        final ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, error, ex);
        return buildResponseEntity(apiError);
    }

    // 405

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(final HttpRequestMethodNotSupportedException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        // logger.info(ex.getClass().getName());
        //
        final StringBuilder builder = new StringBuilder();
        builder.append(ex.getMethod());
        builder.append(" method is not supported for this request. Supported methods are ");
        builder.append(Objects.requireNonNull(ex.getSupportedHttpMethods()).stream().map(Enum::name).collect(Collectors.joining()));

        final ApiError apiError = new ApiError(HttpStatus.METHOD_NOT_ALLOWED, builder.toString(), ex);
        return buildResponseEntity(apiError);
    }

    // 415
    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String message = String.format("The mediatype %s is not supported.", Objects.requireNonNull(ex.getContentType()).getType());
        ApiError error = new ApiError(HttpStatus.UNSUPPORTED_MEDIA_TYPE, message, ex);
        return buildResponseEntity(error);
    }

}
