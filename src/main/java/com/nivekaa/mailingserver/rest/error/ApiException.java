package com.nivekaa.mailingserver.rest.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author nivekaa
 * Created 30/03/2020 at 01:15
 * Class com.nivekaa.mailingserver.rest.error.ApiException
 */

public class ApiException extends RuntimeException {
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public static class BadArgumentsException extends RuntimeException {
        public BadArgumentsException(String message) {
            super(message);
        }
    }
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public static class InternalException extends RuntimeException {

        public InternalException(String message) {
            super(message);
        }
    }
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public static class ResourceNotFoundException extends RuntimeException {
        public ResourceNotFoundException(String message) {
            super(message);
        }
    }
}
