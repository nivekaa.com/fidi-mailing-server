package com.nivekaa.mailingserver.rest.controllers;

import com.nivekaa.mailingserver.configs.ConfigMailingConfiguration;
import com.nivekaa.mailingserver.model.Mail;
import com.nivekaa.mailingserver.model.SimpleMailWithAttachment;
import com.nivekaa.mailingserver.rest.util.FilesUtil;
import com.nivekaa.mailingserver.service.dto.ResponseDTO;
import com.nivekaa.mailingserver.service.dto.SimpleMailDTO;
import com.nivekaa.mailingserver.service.impl.MailServiceImpl;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Generated;
import java.io.IOException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author nivekaa
 * Created 22/02/2020 at 21:59
 * Class com.nivekaa.mailingserver.rest.controllers.SimpleMailController
 */

@RestController
@RequestMapping("simple")
@Api(tags = "Simple mail sender", value = "simple")
@Generated(value = {"org.openapitools.codegen.languages.SpringCodegen"})
public class SimpleMailController {

    private final MailServiceImpl mailService;
    private final ConfigMailingConfiguration configMailingConfiguration;

    public SimpleMailController(MailServiceImpl mailService, ConfigMailingConfiguration configMailingConfiguration) {
        this.mailService = mailService;
        this.configMailingConfiguration = configMailingConfiguration;
    }

    @ApiOperation(nickname = "simple", consumes = APPLICATION_JSON_VALUE, value = "Send a simple mail with content text or html string parser")
    @ApiResponse(code = 200, message = "Mail successfully sended")
    @PostMapping("send")
    public ResponseEntity<?> simple(
            @RequestHeader("X-ENTERPRISE-ID") Long eId,
            @RequestHeader("X-USER-ID") Long uId,
            @RequestBody SimpleMailDTO mailDTO){
        StringBuilder buffer = new StringBuilder();
        //Mail mail = mailDTO.toEntity();

        if (StringUtils.isEmpty(mailDTO.getTo())) {
            buffer.append("Parameter `to` is required")
                    .append("\n");
        }
        if (StringUtils.isEmpty(mailDTO.getContent())) {
            buffer.append("Parameter `content` is required")
                    .append("\n");
        }
        if (buffer.length() != 0) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(buffer.toString());
        }
        mailDTO.setEntrepriseId(eId);
        Mail mail = mailDTO.toEntity();
        mail.setCreatedBy(uId);
        mailService.simple(mail, true);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(ResponseDTO.builder()
                        .message("Successfull")
                        .status(true)
                        .build());
    }


    @ApiOperation(nickname = "simpleWithAttachment", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_VALUE, value = "Send a simple mail with attachment")
    @ApiResponse(code = 200, message = "Mail successfully sended")
    @PostMapping(value = "send-with-attachment", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public ResponseEntity<?> simpleWithAttachment(
            @RequestHeader("X-ENTERPRISE-ID") Long eId,
            @RequestHeader("X-USER-ID") Long uId,
            SimpleMailWithAttachment mailWithAttachment) throws IOException {
        StringBuilder buffer = new StringBuilder();
        SimpleMailDTO mail = mailWithAttachment.getMail();
        //Map<String, Object> map = mailWithAttachment.getData();
        MultipartFile[] multipartFiles = mailWithAttachment.getFiles();
        boolean asHtml = false;
        String lang = "fr";
        if (StringUtils.isEmpty(mail.getTo())){
            buffer.append("Parameter `to` is required")
                    .append("\n");
        }
        if (StringUtils.isEmpty(mail.getContent())){
            buffer.append("Parameter `content` is required")
                    .append("\n");
        }
        if (buffer.length() != 0){
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    //.badRequest()
                    .body(buffer.toString());
        }

        if (FilesUtil.reachedMaxAttachments(multipartFiles, configMailingConfiguration)){
            return ResponseEntity
                    .badRequest()
                    .body(String.format("You had reached max attachments. The max is %d", configMailingConfiguration.getMaxAttachments()));
        }

        if (FilesUtil.maxFileSizeReached(multipartFiles, configMailingConfiguration)){
            return ResponseEntity
                    .badRequest()
                    .body(String.format("You had reached max size file. The actual size is %.3fMo. The max size is %d Mb", FilesUtil.allFileSize(multipartFiles), configMailingConfiguration.getMaxSizeFilesUpload()));
        }

        Mail entity = mail.toEntity();
        entity.setEntrepriseId(eId);
        entity.setCreatedBy(uId);

        InputStreamSource[] files = new InputStreamSource[multipartFiles.length];
        String[] name = new String[multipartFiles.length];
        for (int i = 0; i < multipartFiles.length; i++) {
            name[i] = multipartFiles[i].getOriginalFilename();
            files[i] = new ByteArrayResource(multipartFiles[i].getBytes());
        }

        System.out.println("-----------------------------------");
        System.out.println(entity);
        System.out.println("-----------------------------------");

        mailService.simpleWithAttachments(entity, true, files, name);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(ResponseDTO.builder()
                        .message("Successfull")
                        .status(true)
                        .build());
    }
}
