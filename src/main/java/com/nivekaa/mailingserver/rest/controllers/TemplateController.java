package com.nivekaa.mailingserver.rest.controllers;

import com.nivekaa.mailingserver.Util;
import com.nivekaa.mailingserver.rest.error.NotFoundException;
import com.nivekaa.mailingserver.service.TemplateResolverService;
import com.nivekaa.mailingserver.service.TemplateService;
import com.nivekaa.mailingserver.service.dto.TemplateDTO;
import com.nivekaa.mailingserver.service.dto.TemplateFieldDTO;
import com.nivekaa.mailingserver.service.dto.TemplateFormatedDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author kevin
 * Created 10/10/2020 at 02:55
 * Class com.nivekaa.mailingserver.rest.controllers.TemplateController
 */

@RestController
@RequestMapping("template")
@Slf4j
@RequiredArgsConstructor
public class TemplateController {
    private final TemplateService templateService;
    private final TemplateResolverService templateResolverService;

    @GetMapping("/formated/{codeOrId}")
    public ResponseEntity<TemplateFormatedDTO> getTemplateFormated(
            @RequestHeader(Util.HEADER_ENTERPRISE_ID_KEY) Long eId,
            @PathVariable("codeOrId") String codeOrId,
            @RequestParam Map<String, Object> query) throws NotFoundException {
        log.debug("Controller#getTemplateFormater: {}", codeOrId);
        System.err.println(query);
        TemplateFormatedDTO result = templateResolverService.getTemplateContentAsString(codeOrId, query);
        return ResponseEntity
                .ok(result);
    }

    @PostMapping("/create")
    public ResponseEntity<TemplateDTO> createTemplate(
            @RequestHeader(Util.HEADER_ENTERPRISE_ID_KEY) Long eId,
            @RequestHeader(Util.HEADER_USER_ID_KEY) Long uId,
            @RequestBody TemplateDTO templateDTO) {
        log.debug("Controller#saveTemplate: {}", templateDTO);
        templateDTO.setEntrepriseId(eId);
        TemplateDTO result = templateService.save(templateDTO);
        return ResponseEntity
                .created(URI.create("/one/"+result.getId()))
                .body(result);
    }

    @PostMapping("/{templateId}/field/create")
    public ResponseEntity<TemplateFieldDTO> createTemplateField(
            @RequestHeader(Util.HEADER_ENTERPRISE_ID_KEY) Long eId,
            @RequestHeader(Util.HEADER_USER_ID_KEY) Long uId,
            @PathVariable("templateId") Long templateId,
            @RequestBody TemplateFieldDTO fieldDTO) {
        log.debug("Controller#Template#saveField: {}", fieldDTO);
        fieldDTO.setEntrepriseId(eId);
        TemplateFieldDTO result = templateService.saveField(fieldDTO, templateId);
        return ResponseEntity
                .ok(result);
    }

    @PutMapping("/{templateId}/field/update")
    public ResponseEntity<TemplateFieldDTO> updateTemplateField(
            @RequestHeader(Util.HEADER_ENTERPRISE_ID_KEY) Long eId,
            @RequestHeader(Util.HEADER_USER_ID_KEY) Long uId,
            @PathVariable("templateId") Long templateId,
            @RequestBody TemplateFieldDTO fieldDTO) throws NotFoundException {
        log.debug("Controller#Template#updateField: {}", fieldDTO);
        fieldDTO.setEntrepriseId(eId);
        TemplateFieldDTO result = templateService.updateField(fieldDTO, templateId);
        return ResponseEntity
                .ok(result);
    }

    @PutMapping("/update/{code}")
    public ResponseEntity<TemplateDTO> updateTemplate(
            @RequestHeader(Util.HEADER_USER_ID_KEY) Long uId,
            @PathVariable("code") String code,
            @RequestBody TemplateDTO templateDTO) throws NotFoundException {
        log.debug("Controller#updateTemplate: {}", templateDTO);
        TemplateDTO result = templateService.update(code, templateDTO);
        return ResponseEntity
                .accepted()
                .body(result);
    }

    @GetMapping("/one/{code}")
    public ResponseEntity<TemplateDTO> getOneTemplate(
            @PathVariable("code") String code,
            @RequestHeader(Util.HEADER_ENTERPRISE_ID_KEY) Long eId) throws NotFoundException {
        log.debug("Controller#findOneTemplate: {}", code);
        Optional<TemplateDTO> result = templateService.findOne(code, eId);
        return result.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound()
                .build());
    }

    @GetMapping("/all")
    public ResponseEntity<List<TemplateDTO>> getAllTemplate(
            @RequestHeader(Util.HEADER_ENTERPRISE_ID_KEY) Long eId) throws NotFoundException {
        log.debug("Controller#findAllTemplate");
        List<TemplateDTO> result = templateService.findAll(eId);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/{templateId}/field/all")
    public ResponseEntity<List<TemplateFieldDTO>> getAllTemplateField(
            @RequestHeader(Util.HEADER_ENTERPRISE_ID_KEY) Long eId,
            @PathVariable("templateId") Long templateId
    ) throws NotFoundException {
        log.debug("Controller#template#findAllTemplateField");
        List<TemplateFieldDTO> result = templateService.findFieldByTemplateId(eId, templateId);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<List<TemplateDTO>> deleteTemplate(
            @PathVariable("id") Long id,
            @RequestHeader(Util.HEADER_ENTERPRISE_ID_KEY) Long eId) throws NotFoundException {
        log.debug("Controller#deleteTemplate: {}", id);
        List<TemplateDTO> result = templateService.findAll(eId);
        return ResponseEntity.ok(result);
    }
}
