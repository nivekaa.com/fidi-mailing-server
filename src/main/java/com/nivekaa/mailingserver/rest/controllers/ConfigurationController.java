package com.nivekaa.mailingserver.rest.controllers;

import com.nivekaa.mailingserver.Util;
import com.nivekaa.mailingserver.rest.error.ApiException;
import com.nivekaa.mailingserver.service.MailingConfigurationService;
import com.nivekaa.mailingserver.service.dto.MConfigDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Objects;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author nivekaa
 * Created 29/03/2020 at 23:17
 * Class com.nivekaa.mailingserver.rest.controllers.ConfigurationController
 */

@Api(tags = "Mail configuration ")
@RequestMapping("config")
@RestController
public class ConfigurationController {

    private final Logger log = LoggerFactory.getLogger(ConfigurationController.class);
    private final MailingConfigurationService service;

    public ConfigurationController(MailingConfigurationService service) {
        this.service = service;
    }

    @ApiOperation(nickname = "find", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE, value = "Find configuration mail per entreprise")
    @ApiResponse(code = 200, message = "Mail successfully scheduled")
    @GetMapping(value = "find/{entrepriseId}")
    public ResponseEntity<?> findConfig(
            @RequestHeader("X-ENTERPRISE-ID") Long eId,
            @RequestHeader("X-USER-ID") Long uId,
            @PathVariable(required = true) Long entrepriseId) throws Throwable {
        if (Objects.isNull(eId)){
            return ResponseEntity
                    .badRequest()
                    .body("The Entreprise is missing in Header");
        }
        if (Objects.isNull(uId)){
            return ResponseEntity
                    .badRequest()
                    .body("The user is missing in Header");
        }

        MConfigDTO mConfigDTO = service.findByEnterprise(entrepriseId);
        if (mConfigDTO == null){
            return ResponseEntity.badRequest()
                    .body(null);
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(mConfigDTO);
    }

    @ApiOperation(nickname = "saveConfig", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE, value = "Save configuration")
    @ApiResponse(code = 200, message = "Configuration successfully saved")
    @PostMapping(value = "save", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> saveConfig(
            @RequestHeader(Util.HEADER_ENTERPRISE_ID_KEY) Long eId,
            @RequestHeader(Util.HEADER_USER_ID_KEY) Long uId,
            @RequestBody MConfigDTO mConfigDTO) throws IOException, ApiException {
        log.debug("Controller#saveConfig: {}", mConfigDTO);
        StringBuilder builder = new StringBuilder();
        builder.append("This fields is required:");
        boolean hasError = false;
        if (StringUtils.isEmpty(mConfigDTO.getFromEmail())){
            builder.append(" from mail(Source),");
            hasError = true;
        }
        if (StringUtils.isEmpty(mConfigDTO.getFromName())){
            builder.append(" from name (personal name),");
            hasError = true;
        }

        if (hasError){
            //builder.delete(0, builder.length()-1);
            throw new ApiException.BadArgumentsException(builder.toString());
        }
        mConfigDTO.setEntrepriseId(eId);
        MConfigDTO res = service.save(mConfigDTO);
        if (res == null){
            return ResponseEntity.badRequest()
                    .build();
        }

        return ResponseEntity
                .ok()
                .body(res);
    }

    @ApiOperation(nickname = "updateConfig", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE, value = "Save configuration")
    @ApiResponse(code = 200, message = "Configuration successfully updated")
    @PostMapping(value = "update")
    public ResponseEntity<?> updateConfig(
            @RequestHeader(Util.HEADER_ENTERPRISE_ID_KEY) Long eId,
            //@RequestHeader(Util.HEADER_USER_ID_KEY) Long uId,
            @RequestBody MConfigDTO mConfigDTO) throws IOException {
        log.debug("Controller#saveConfig: {}", mConfigDTO);
        System.out.println("---------------------------------");
        System.out.println(mConfigDTO);
        System.out.println("---------------------------------");
        StringBuilder builder = new StringBuilder();
        builder.append("This fields is required:");

        if (mConfigDTO.getId() == null){
            throw new ApiException.BadArgumentsException("Invalid Id");
        }

        if (!mConfigDTO.getEntrepriseId().equals(eId)){
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .build();
        }

        mConfigDTO.setEntrepriseId(eId);
        mConfigDTO = service.save(mConfigDTO);
        if (mConfigDTO == null){
            return ResponseEntity.badRequest()
                    .build();
        }

        return ResponseEntity
                .ok()
                .body(mConfigDTO);
    }
}
