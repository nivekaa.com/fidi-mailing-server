package com.nivekaa.mailingserver.rest.controllers;

import com.nivekaa.mailingserver.Util;
import com.nivekaa.mailingserver.configs.ConfigMailingConfiguration;
import com.nivekaa.mailingserver.configs.SokoConfiguration;
import com.nivekaa.mailingserver.jobs.ScheduleEmailJob;
import com.nivekaa.mailingserver.model.FileStore;
import com.nivekaa.mailingserver.model.Mail;
import com.nivekaa.mailingserver.model.MailWithAttachment;
import com.nivekaa.mailingserver.model.ScheduleEmailResponse;
import com.nivekaa.mailingserver.rest.util.FilesUtil;
import com.nivekaa.mailingserver.service.FileStoreService;
import com.nivekaa.mailingserver.service.MailEntityService;
import com.nivekaa.mailingserver.service.dto.MailDTO;
import com.nivekaa.mailingserver.service.impl.MailServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.apache.commons.lang3.StringUtils;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author nivekaa
 * Created 23/02/2020 at 18:54
 * Class com.nivekaa.mailingserver.rest.controllers.MailScheduleController
 */

@Api(tags = "Programmation des mails")
@RequestMapping("schedule")
@RestController
public class MailScheduleController {
    //Save the uploaded file to this folder
    private Logger logger = LoggerFactory.getLogger(MailScheduleController.class);
    private final Scheduler scheduler;
    private final MailEntityService mailEntityService;
    private final FileStoreService fileStoreService;
    private final ConfigMailingConfiguration configMailingConfiguration;
    public MailScheduleController(Scheduler scheduler, MailEntityService mailEntityService, FileStoreService fileStoreService, ConfigMailingConfiguration configMailingConfiguration) {
        this.scheduler = scheduler;
        this.mailEntityService = mailEntityService;
        this.fileStoreService = fileStoreService;
        this.configMailingConfiguration = configMailingConfiguration;
    }

    @ApiOperation(nickname = "scheduleWithAttachment", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_VALUE, value = "Scheduling a simple mail with attachment")
    @ApiResponse(code = 200, message = "Mail successfully scheduled")
    @PostMapping(value = "schedule-with-attachments", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public ResponseEntity<?> scheduleWithAttachment(
            @RequestHeader("X-ENTERPRISE-ID") Long eId,
            @RequestHeader("X-USER-ID") Long uId,
            MailWithAttachment mailWithAttachment) throws Exception {
        try {
            StringBuilder buffer = new StringBuilder();
            Mail mail = mailWithAttachment.getMail();
            // Map<String, Object> map = mailWithAttachment.getData();
            MultipartFile[] multipartFiles = mailWithAttachment.getFiles();
            ZonedDateTime dateTime = ZonedDateTime.of(LocalDateTime.parse(mail.getDateTime(), Util.DATE_TIME_FORMATTER), ZoneId.of(mail.getTimeZone()));
            if (dateTime.isBefore(ZonedDateTime.now())) {
                ScheduleEmailResponse scheduleEmailResponse = new ScheduleEmailResponse(false,
                        "dateTime must be after current time");
                return ResponseEntity.badRequest().body(scheduleEmailResponse);
            }

            if (StringUtils.isEmpty(mail.getTo())) {
                buffer.append("Parameter `to` is required")
                        .append("\n");
            }
            if (StringUtils.isEmpty(mail.getFrom())) {
                buffer.append("Parameter `from` is required")
                        .append("\n");
            }
            if (buffer.length() != 0) {
                return ResponseEntity
                        .status(HttpStatus.BAD_REQUEST)
                        .build();
            }

            if (FilesUtil.reachedMaxAttachments(multipartFiles, configMailingConfiguration)){
                return ResponseEntity
                        .badRequest()
                        .body(String.format("You had reached max attachments. The max is %d", configMailingConfiguration.getMaxAttachments()));
            }

            if (FilesUtil.maxFileSizeReached(multipartFiles, configMailingConfiguration)){
                return ResponseEntity
                        .badRequest()
                        .body(String.format("You had reached max size file. The actual size is %.3f. The max size is %d Mb", FilesUtil.allFileSize(multipartFiles), configMailingConfiguration.getMaxSizeFilesUpload()));
            }

            mail.setContent(StringUtils.isEmpty(mail.getContent()) ? " " : mail.getContent());
            mail.setLastModifiedBy(uId);
            mail.setCreatedBy(uId);
            mail.setEntrepriseId(eId);

            JobDetail jobDetail = buildJobDetail(mail, multipartFiles, eId, uId);
            Trigger trigger = buildJobTrigger(jobDetail, dateTime);
            scheduler.scheduleJob(jobDetail, trigger);

            ScheduleEmailResponse scheduleEmailResponse = new ScheduleEmailResponse(true,
                    jobDetail.getKey().getName(), jobDetail.getKey().getGroup(), "Email Scheduled Successfully!");

            return ResponseEntity.ok(scheduleEmailResponse);

        }catch (SchedulerException e){
            logger.error(e.getMessage());
            ScheduleEmailResponse scheduleEmailResponse = new ScheduleEmailResponse(false,
                    "Error scheduling email. Please try later!" + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(scheduleEmailResponse);
        }
    }



    @ApiOperation(nickname = "simple",
            consumes = APPLICATION_JSON_VALUE,
            value = "Schedule a simple mail with content text or html string parser"
    )
    @ApiResponse(code = 200, message = "Mail successfully scheduled")
    @PostMapping("schedule")
    public ResponseEntity<?> simple(
            @RequestHeader("X-ENTERPRISE-ID") Long eId,
            @RequestHeader("X-USER-ID") Long uId,
            @RequestBody MailDTO mailDTO){
        StringBuilder buffer = new StringBuilder();
        Mail mail = mailDTO.toEntity();


        System.out.println("-----------------------------------");
        System.out.println(mailDTO);
        System.out.println("-----------------------------------");

        AtomicBoolean okZoneId = new AtomicBoolean(false);
        ZoneId.getAvailableZoneIds().forEach(s -> {
            if (s.toLowerCase().equals(mail.getTimeZone())){
                mail.setTimeZone(s);
                okZoneId.set(true);
            }
        });

        if (!okZoneId.get()){
            if (buffer.length() != 0) {
                return ResponseEntity
                        .status(HttpStatus.BAD_REQUEST)
                        .body("Invalid timeZone "+ mail.getTimeZone());
            }
        }


        ZonedDateTime dateTime = ZonedDateTime.of(LocalDateTime.parse(mail.getDateTime(), Util.DATE_TIME_FORMATTER), ZoneId.of(mail.getTimeZone()));
        if (dateTime.isBefore(ZonedDateTime.now())) {
            ScheduleEmailResponse scheduleEmailResponse = new ScheduleEmailResponse(false,
                    "dateTime must be after current time");
            return ResponseEntity.badRequest().body(scheduleEmailResponse);
        }

        if (StringUtils.isEmpty(mail.getTo())) {
            buffer.append("Parameter `to` is required")
                    .append("\n");
        }
        if (StringUtils.isEmpty(mail.getContent())) {
            buffer.append("Parameter `content` is required")
                    .append("\n");
        }
        if (buffer.length() != 0) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(buffer.toString());
        }
        mail.setContent(StringUtils.isEmpty(mail.getContent()) ? " " : mail.getContent());
        mail.setLastModifiedBy(uId);
        mail.setCreatedBy(uId);
        mail.setEntrepriseId(eId);
        try {
            JobDetail jobDetail = buildJobDetail(mail, null, eId, uId);
            Trigger trigger = buildJobTrigger(jobDetail, dateTime);
            scheduler.scheduleJob(jobDetail, trigger);

            ScheduleEmailResponse scheduleEmailResponse = new ScheduleEmailResponse(true,
                    jobDetail.getKey().getName(), jobDetail.getKey().getGroup(), "Email Scheduled Successfully!");

            return ResponseEntity.ok(scheduleEmailResponse);

        } catch (SchedulerException | IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            ScheduleEmailResponse scheduleEmailResponse = new ScheduleEmailResponse(false,
                    "Error scheduling email. Please try later!" + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(scheduleEmailResponse);
        }
    }

    private JobDetail buildJobDetail(Mail scheduleEmailRequest, MultipartFile[] multipartFiles, Long eId, Long uId) throws IOException {
        JobDataMap jobDataMap = new JobDataMap();
        MailDTO mailDTO = mailEntityService.save(scheduleEmailRequest);
        jobDataMap.put("id", mailDTO.getId());
        jobDataMap.put("to", mailDTO.getTo());
        //jobDataMap.put("from", mailDTO.getFrom());
        jobDataMap.put("entrepriseId", eId);
        if (multipartFiles != null) {
            Mail m = new Mail();
            m.setId(mailDTO.getId());
            m.setFrom(mailDTO.getFrom());
            m.setTo(mailDTO.getTo());
            for (MultipartFile multipartFile : multipartFiles) {
                // multipartFile.getResource().getFile();
                File f = FilesUtil.copyMultipartToFile(multipartFile);
                FileStore file = new FileStore();
                file.setLastModifiedBy(uId);
                file.setCreatedBy(uId);
                file.setEntrepriseId(eId);
                file.setFilename(multipartFile.getOriginalFilename());
                file.setMail(m);
                file.setLocation(f.getPath());
                file.setFileType(f.getAbsolutePath());
                fileStoreService.save(file);
            }
        }

        return JobBuilder.newJob(ScheduleEmailJob.class)
                .withIdentity(UUID.randomUUID().toString(), "email-jobs")
                .withDescription("Send Email Job")
                .usingJobData(jobDataMap)
                .storeDurably()
                .build();
    }
    private Trigger buildJobTrigger(JobDetail jobDetail, ZonedDateTime startAt) {
        return TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .withIdentity(jobDetail.getKey().getName(), "email-triggers")
                .withDescription("Send Email Trigger")
                .startAt(Date.from(startAt.toInstant()))
                .withSchedule(
                        SimpleScheduleBuilder
                                .simpleSchedule()
                                .withMisfireHandlingInstructionFireNow()
                )
                .build();
    }
}
