package com.nivekaa.mailingserver.rest.util;

import com.nivekaa.mailingserver.configs.ConfigMailingConfiguration;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * @author nivekaa
 * Created 23/02/2020 at 16:19
 * Class com.nivekaa.mailingserver.rest.util.FilesUtil
 */

@Component
public class FilesUtil {
    public static final String path= "uploads"+File.separator+"email-attachments"+File.separator;
    public static final String pathTmp= "uploads"+File.separator+"tmp"+File.separator;
    public static File moveAndStoreFile(MultipartFile file) {
        System.out.println("------------------"+ path+file.getOriginalFilename()+"-----------------------");
        File convFile = new File(path+file.getOriginalFilename());
        try {
            convFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(file.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return convFile;
    }

    private static String filesize_in_megaBytes(File file) {
        return (double) file.length()/(1024*1024)+" mb";
    }

    private static String filesize_in_kiloBytes(File file) {
        return (double) file.length()/1024+"  kb";
    }

    private static String filesize_in_Bytes(File file) {
        return file.length()+" bytes";
    }

    public static File copyMultipartToFile(MultipartFile multipartFile){
        File file = new File(path+multipartFile.getOriginalFilename());
        try {
            multipartFile.transferTo(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static File copyMultipartToTmpFile(MultipartFile multipartFile){
        File file = new File(pathTmp+multipartFile.getOriginalFilename());
        try {
            multipartFile.transferTo(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static int countFiles(MultipartFile[] multipartFiles){
        if (multipartFiles!=null && multipartFiles.length!=0){
            return multipartFiles.length;
        }
        else
            return 0;
    }

    public static boolean reachedMaxAttachments(MultipartFile[] multipartFiles, ConfigMailingConfiguration configuration){
        return configuration.getMaxAttachments() < countFiles(multipartFiles);
    }

    public static double allFileSize(MultipartFile[] multipartFiles){
        double size = 0;
        for (MultipartFile multipart: multipartFiles) {
            size += (double) multipart.getSize()/(1024*1024);
        }
        return size;
    }

    public static boolean maxFileSizeReached(MultipartFile[] multipartFiles, ConfigMailingConfiguration configuration) {
        return allFileSize(multipartFiles) > configuration.getMaxSizeFilesUpload();
    }



    public static String readFileToString(String path) {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource(path);
        return asString(resource);
    }

    public static String asString(Resource resource) {
        try (Reader reader = new InputStreamReader(resource.getInputStream(), UTF_8)) {
            return FileCopyUtils.copyToString(reader);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

}
