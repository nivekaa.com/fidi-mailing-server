package com.nivekaa.mailingserver.rest.util;

import com.nivekaa.mailingserver.Util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author kevin
 * Created 09/10/2020 at 22:46
 * Class com.nivekaa.mailingserver.rest.util.CdrHelper
 */

public class CdrHelper {
    public static final String _CDR_FOR_ERROR = "CUSTOM_ERROR";
    public static String formatCdrLog(String... values){
        List<String> list = Arrays.asList(values);
        list.replaceAll(el -> el!=null ? el.replaceAll("\n", "") : "");
        String format = String.join(";", Collections.nCopies(list.size(), "%s"));
        return String.format(format, values);
    }

    public static String successFormatCdrLog(String method, Object o) {
        return formatCdrLog(method, "SUCCESS", Util.objToJson(o));
    }

    public static String errorFormatCdrLog(String method, Object o) {
        return formatCdrLog(method, "ERROR", Util.objToJson(o));
    }

    public static String warnFormatCdrLog(String method, Object o) {
        return formatCdrLog(method, "WARNING", Util.objToJson(o));
    }

    public static String infoFormatCdrLog(String method, Object o) {
        return formatCdrLog(method, "INFO", Util.objToJson(o));
    }
}
