package com.nivekaa.mailingserver.repositories;

import com.nivekaa.mailingserver.model.MailingConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author nivekaa
 * Created 29/03/2020 at 22:23
 * Class com.nivekaa.mailingserver.repositories.MailingConfigurationRepository
 */

@Repository
public interface MailingConfigurationRepository extends JpaRepository<MailingConfiguration, Long> {
    Optional<MailingConfiguration> findByEntrepriseId(Long entrepriseId);
    Optional<MailingConfiguration> findByIdAndEntrepriseId(Long id, Long entrepriseId);
}
