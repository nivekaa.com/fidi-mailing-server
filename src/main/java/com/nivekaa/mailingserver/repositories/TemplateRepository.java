package com.nivekaa.mailingserver.repositories;

import com.nivekaa.mailingserver.model.Template;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author kevin
 * Created 10/10/2020 at 02:47
 * Class com.nivekaa.mailingserver.repositories.TemplateRepository
 */


@Repository
public interface TemplateRepository extends JpaRepository<Template, Long> {
    List<Template> findByEntrepriseId(Long enterpriseId);
    Template findByIdAndEntrepriseId(Long id, Long enterpriseId);
    Template findByCodeAndEntrepriseId(String code, Long enterpriseId);
    Optional<Template> findByCode(String code);

    void deleteByEntrepriseIdAndId(Long enterprise, Long id);
}
