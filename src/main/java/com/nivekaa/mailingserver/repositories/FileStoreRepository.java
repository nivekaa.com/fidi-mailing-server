package com.nivekaa.mailingserver.repositories;

import com.nivekaa.mailingserver.model.FileStore;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * @author nivekaa
 * Created 23/02/2020 at 20:01
 * Class com.nivekaa.mailingserver.repositories.FileStoreRepository
 */

@Repository
public interface FileStoreRepository extends JpaRepository<FileStore, Long> {
    @Query(nativeQuery = true, value = "SELECT afs FROM attachment_file_store afs INNER JOIN log_mail lm on afs.log_mail_id = lm.id WHERE afs.entreprise_id=:eId AND lm.id=:id")
    List<FileStore> findAllByEntrepriseIdAndMail_Id(@Param("eId") Long eId, @Param("id") Long mailId);
    Set<FileStore> findByMail_IdAndEntrepriseId(Long mailId, Long entrepriseId);
    Set<FileStore> findAllByMail_IdAndEntrepriseId(Long mailId, Long entrepriseId);
}
