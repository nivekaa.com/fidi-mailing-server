package com.nivekaa.mailingserver.repositories;

import com.nivekaa.mailingserver.model.TemplateField;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author kevin
 * Created 13/10/2020 at 02:50
 * Class com.nivekaa.mailingserver.repositories.TemplateFieldRepository
 */

@Repository
public interface TemplateFieldRepository extends JpaRepository<TemplateField, Long> {
    List<TemplateField> findByEntrepriseIdAndTemplate_Id(Long enterpriseId, Long templateId);
    Optional<TemplateField> findByNameAndTemplate_Id(String name, Long tempateId);
}
