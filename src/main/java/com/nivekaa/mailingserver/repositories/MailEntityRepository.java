package com.nivekaa.mailingserver.repositories;

import com.nivekaa.mailingserver.model.Mail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author nivekaa
 * Created 23/02/2020 at 19:59
 * Class com.nivekaa.mailingserver.repositories.MailEntityRepository
 */

@Repository
public interface MailEntityRepository extends JpaRepository<Mail, Long> {
    Optional<Mail> findByIdAndEntrepriseId(Long id, Long entrepriseId);
    Page<Mail> findAllByEntrepriseId(Long entrepriseId, Pageable page);
    void deleteByIdAndEntrepriseId(Long id, Long eid);

}
