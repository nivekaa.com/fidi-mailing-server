package com.nivekaa.mailingserver;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nivekaa.mailingserver.service.TemplateResolverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@EnableDiscoveryClient
@EnableEurekaClient
@EnableFeignClients
public class MailingServerApplication extends SpringBootServletInitializer implements CommandLineRunner {
    @Autowired
    TemplateResolverService templateResolverService;
    public static void main(String[] args) {
        SpringApplication.run(MailingServerApplication.class, args);
    }

    @Bean
    public ObjectMapper objectMapper(){
        return new ObjectMapper()
                .configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true)
                .configure(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true)
                .configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true)
                .configure(JsonParser.Feature.ALLOW_YAML_COMMENTS, true);
    }

    @Override
    public void run(String... args) throws Exception {
       /*Map<String, Object> attrs = new HashMap<>();
        attrs.put("taskName", "TEST TEST TEST");
        attrs.put("taskId", "87897K9-------------JHKJH98897987");
        attrs.put("projectName", "PROJECT 0001");
        attrs.put("projectId", "09-------------988980980980098");
        attrs.put("userName", "NIVEKA LARA");
        attrs.put("title","Niveka vous a mentionné");
        attrs.put("taskDesc","Donec rutrum congue leo eget malesuada. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Proin eget tortor risus.");
        TemplateFormatedDTO res = templateResolverService.getTemplateContentAsString("TEMPL-MENTION", attrs, Locale.ENGLISH.toString());
        System.err.println(res.getHtml());
        System.exit(0);*/
    }
}
