package com.nivekaa.mailingserver.service;

import com.nivekaa.mailingserver.model.MailingConfiguration;
import com.nivekaa.mailingserver.repositories.MailingConfigurationRepository;
import com.nivekaa.mailingserver.rest.error.NotFoundException;
import com.nivekaa.mailingserver.rest.util.CdrHelper;
import com.nivekaa.mailingserver.service.dto.MConfigDTO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.nivekaa.mailingserver.Util._CDR_OPERATIONS;

/**
 * @author nivekaa
 * Created 29/03/2020 at 22:30
 * Class com.nivekaa.mailingserver.service.MailingConfigurationService
 */

@Service
public class MailingConfigurationService {
    private Logger log = LoggerFactory.getLogger(_CDR_OPERATIONS);
    private final MailingConfigurationRepository configurationRepository;

    public MailingConfigurationService(MailingConfigurationRepository configurationRepository) {
        this.configurationRepository = configurationRepository;
    }

    public MConfigDTO save(MConfigDTO dto){
        log.debug("Saving config: {}", dto);
        MConfigDTO mConfigDTO = configurationRepository.save(dto.toEntity())
                .toDto();
        log.info(CdrHelper.successFormatCdrLog("saveConfigDto", mConfigDTO));
        return mConfigDTO;
    }

    public MConfigDTO update(MConfigDTO dto){
        return Optional.ofNullable(configurationRepository.findByIdAndEntrepriseId(dto.getId(), dto.getEntrepriseId()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(entity -> {
                    //if (StringUtils.isNotEmpty(dto.getFromEmail()))
                        entity.setFromEmail(dto.getFromEmail());
                    // if (StringUtils.isNotEmpty(dto.getFromName()))
                        entity.setFromName(dto.getFromName());
                    //if (StringUtils.isNotEmpty(dto.getFooterContent()))
                        entity.setFooterContent(dto.getFooterContent());
                    //if (StringUtils.isNotEmpty(dto.getHeaderContent()))
                        entity.setHeaderContent(dto.getHeaderContent());
                    // if (StringUtils.isNotEmpty(dto.getOutgoingMethod()))
                        entity.setOutgoingMethod(dto.getOutgoingMethod());
                    configurationRepository.save(entity);
                    log.info(CdrHelper.successFormatCdrLog("updateConfigDto", entity));
                    return entity;
                })
                .orElse(dto.toEntity())
                .toDto();
    }

    public MConfigDTO findByEnterprise(Long eId) throws Throwable {
        return configurationRepository.findByEntrepriseId(eId)
                .map(MailingConfiguration::toDto)
                .orElseThrow(() -> {
                    log.info(CdrHelper.infoFormatCdrLog("findConfigByEnterprise", eId));
                    return new NotFoundException();
                });
    }

    public MConfigDTO find(Long eId, Long id){
        log.info(CdrHelper.infoFormatCdrLog("findConfigByEnterpriseAndId", id));
        return Optional.ofNullable(configurationRepository
                .findByIdAndEntrepriseId(id, eId))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .get()
                .toDto();
    }
}
