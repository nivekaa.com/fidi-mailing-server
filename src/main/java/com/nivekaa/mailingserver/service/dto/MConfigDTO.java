package com.nivekaa.mailingserver.service.dto;

import com.nivekaa.mailingserver.model.MailingConfiguration;
import lombok.*;

/**
 * @author nivekaa
 * Created 29/03/2020 at 22:33
 * Class com.nivekaa.mailingserver.service.dto.MConfigDTO
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
public class MConfigDTO {
    private Long id;
    private Long entrepriseId;
    private String fromEmail;
    private String fromName;
    private String footerContent;
    private String headerContent;
    private String outgoingMethod;

    public MailingConfiguration toEntity(){
        return MailingConfiguration.builder()
                .entrepriseId(getEntrepriseId())
                .footerContent(getFooterContent())
                .headerContent(getHeaderContent())
                .fromEmail(getFromEmail())
                .fromName(getFromName())
                .outgoingMethod(getOutgoingMethod())
                .build();
    }
}
