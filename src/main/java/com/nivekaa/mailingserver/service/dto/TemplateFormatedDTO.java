package com.nivekaa.mailingserver.service.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @author kevin
 * Created 16/10/2020 at 22:49
 * Class com.nivekaa.mailingserver.service.dto.TemplateFormatedDTO
 */

@Data
@Builder
public class TemplateFormatedDTO {
    private String html;
}
