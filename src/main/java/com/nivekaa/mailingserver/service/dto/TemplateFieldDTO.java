package com.nivekaa.mailingserver.service.dto;

import com.nivekaa.mailingserver.model.Template;
import com.nivekaa.mailingserver.model.TemplateField;
import lombok.*;

/**
 * @author kevin
 * Created 13/10/2020 at 02:35
 * Class com.nivekaa.mailingserver.service.dto.TemplateFieldDTO
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class TemplateFieldDTO {
    private Long id;
    private Long entrepriseId;
    private String name;
    private String description;
    private String defaultValue;
    private Long templateId;

    public TemplateField toEntity() {
        return TemplateField.builder()
                .id(id)
                .defaultValue(defaultValue)
                .description(description)
                .entrepriseId(entrepriseId)
                .name(name)
                .template(Template.builder()
                        .id(templateId)
                        .build()
                ).build();
    }

    public static TemplateFieldDTO fromEntity(TemplateField entity) {
        return TemplateFieldDTO.builder()
                .id(entity.getId())
                .name(entity.getName())
                .defaultValue(entity.getDefaultValue())
                .description(entity.getDescription())
                //.templateId(entity.getTemplate().getId())
                .entrepriseId(entity.getEntrepriseId())
                .build();
    }
}
