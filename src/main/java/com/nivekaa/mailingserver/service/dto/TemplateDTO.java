package com.nivekaa.mailingserver.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nivekaa.mailingserver.model.Template;
import com.nivekaa.mailingserver.model.TemplateField;
import lombok.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author kevin
 * Created 10/10/2020 at 02:31
 * Class com.nivekaa.mailingserver.service.dto.TemplateDTO
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class TemplateDTO {
    private Long id;
    @JsonIgnore
    private Long entrepriseId;
    private String content;
    private String code;
    private String dataModel;
    private List<TemplateFieldDTO> fields = new ArrayList<>();

    public Template toEntity() {
        return Template.builder()
                .code(code)
                .content(content)
                .entrepriseId(entrepriseId)
                .dataModel(dataModel)
                .fields(
                        fields == null ? new HashSet<>() :
                                fields.stream().map(TemplateFieldDTO::toEntity).collect(Collectors.toSet()))
                .build();
    }

    public static TemplateDTO fromEntity(Template entity) {
        return TemplateDTO.builder()
                .id(entity.getId())
                .code(entity.getCode())
                .content(entity.getContent())
                .dataModel(entity.getDataModel())
                .entrepriseId(entity.getEntrepriseId())
                .fields(
                        entity.getFields() == null ? new ArrayList<>() :
                                entity.getFields().stream().map(TemplateFieldDTO::fromEntity).collect(Collectors.toList()))
                .build();
    }
}
