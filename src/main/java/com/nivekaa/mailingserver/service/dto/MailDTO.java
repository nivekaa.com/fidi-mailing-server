package com.nivekaa.mailingserver.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nivekaa.mailingserver.model.FileStore;
import com.nivekaa.mailingserver.model.Mail;
import lombok.*;

import java.util.Set;

/**
 * @author nivekaa
 * Created 23/02/2020 at 20:03
 * Class com.nivekaa.mailingserver.service.dto.MailDTO
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class MailDTO {
    private Long id;
    @JsonIgnore
    private Long entrepriseId;
    private String from;
    private String to;
    private String cc;
    private String bcc;
    private String subject;
    private boolean noFooter;
    private boolean noHeader;
    private String content;
    @JsonIgnore
    transient private String langKey;
    @JsonIgnore
    transient private Set<FileStore> files;
    private String dateTime;
    private String timeZone;

    public Mail toEntity(){
        return Mail.builder()
                .bcc(getBcc())
                .cc(getCc())
                .content(getContent())
                .dateTime(getDateTime())
                .from(getFrom())
                .subject(getSubject())
                .timeZone(getTimeZone())
                .noFooter(isNoFooter())
                .noHeader(isNoHeader())
                .to(getTo())
                .entrepriseId(getEntrepriseId())
                .id(getId())
                .langKey(getLangKey())
                .build();
    }
}
