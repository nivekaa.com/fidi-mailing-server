package com.nivekaa.mailingserver.service.dto;

import com.nivekaa.mailingserver.model.Mail;
import lombok.*;

/**
 * @author nivekaa
 * Created 23/02/2020 at 20:07
 * Class com.nivekaa.mailingserver.service.dto.FileStoreDTO
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class FileStoreDTO {
    private Long id;
    private String location;
    private Long entrepriseId;
    private String filename;
    private String fileType;
    private Mail mail;
}
