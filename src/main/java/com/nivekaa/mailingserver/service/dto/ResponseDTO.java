package com.nivekaa.mailingserver.service.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author nivekaa
 * Created 04/04/2020 at 15:07
 * Class com.nivekaa.mailingserver.service.dto.ResponseDTO
 */

@Getter
@Setter
@Builder
public class ResponseDTO implements Serializable {
    private String message;
    private boolean status;
}
