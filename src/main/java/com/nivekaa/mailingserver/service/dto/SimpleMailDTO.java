package com.nivekaa.mailingserver.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.nivekaa.mailingserver.model.FileStore;
import com.nivekaa.mailingserver.model.Mail;
import lombok.*;

import java.util.Set;

/**
 * @author nivekaa
 * Created 01/04/2020 at 21:37
 * Class com.nivekaa.mailingserver.service.dto.SimpleMailDTO
 */


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class SimpleMailDTO {
    @JsonIgnore
    transient private Long id;
    @JsonIgnore
    transient private Long entrepriseId;
    transient private String from;
    @JsonProperty(required = true)
    private String to;
    private String cc;
    private String bcc;
    @JsonProperty(required = true)
    private String subject;
    @JsonProperty(defaultValue = "false")
    private boolean noDefaultFooter;
    @JsonProperty(defaultValue = "false")
    private boolean noDefaultHeader;
    @JsonProperty(required = true)
    private String content;

    public Mail toEntity(){
        return Mail.builder()
                .bcc(getBcc())
                .cc(getCc())
                .content(getContent())
                .from(getFrom())
                .subject(getSubject())
                .noFooter(isNoDefaultFooter())
                .noHeader(isNoDefaultFooter())
                .to(getTo())
                .entrepriseId(getEntrepriseId())
                .id(getId())
                .build();
    }
}

