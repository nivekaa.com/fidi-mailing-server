package com.nivekaa.mailingserver.service;

import com.nivekaa.mailingserver.model.Mail;
import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Map;

/**
 * @author dell
 * Created 10/02/2020 at 23:45
 * Interface com.nivekaa.mailingserver.service.IMailService
 */
@Service
public interface IMailService {
    //simple
    void simple(Mail utilities, boolean asHTML);
    void simpleWithAttachments(Mail utilities, boolean asHTML, File[] files);

    //with template
    void withTemplate(Mail utilities, Map<String,Object> variables);
    void withTemplateAndAttachments(Mail utilities,Map<String,Object> variables,File[] files);
}

