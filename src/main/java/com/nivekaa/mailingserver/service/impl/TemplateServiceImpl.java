package com.nivekaa.mailingserver.service.impl;

import com.nivekaa.mailingserver.model.Template;
import com.nivekaa.mailingserver.model.TemplateField;
import com.nivekaa.mailingserver.repositories.TemplateFieldRepository;
import com.nivekaa.mailingserver.repositories.TemplateRepository;
import com.nivekaa.mailingserver.rest.error.NotFoundException;
import com.nivekaa.mailingserver.rest.util.CdrHelper;
import com.nivekaa.mailingserver.service.TemplateService;
import com.nivekaa.mailingserver.service.dto.TemplateDTO;
import com.nivekaa.mailingserver.service.dto.TemplateFieldDTO;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.nivekaa.mailingserver.Util._CDR_OPERATIONS;

/**
 * @author kevin
 * Created 10/10/2020 at 03:12
 * Class com.nivekaa.mailingserver.service.impl.TemplateServiceImpl
 */

@Service
@Transactional
@RequiredArgsConstructor
public class TemplateServiceImpl implements TemplateService {
    private final TemplateRepository templateRepository;
    private final TemplateFieldRepository templateFieldRepository;
    private static final Logger log = LoggerFactory.getLogger(_CDR_OPERATIONS);

    @Override
    public TemplateDTO save(TemplateDTO dto) {
        Template saved = templateRepository.save(dto.toEntity());
        log.info(CdrHelper.successFormatCdrLog("createTemplate", saved));
        return TemplateDTO.fromEntity(saved);
    }

    @Override
    public TemplateDTO update(String code, TemplateDTO dto) throws NotFoundException {
        return templateRepository.findByCode(code)
                .map(template -> {
                    template.setContent(dto.getContent());
                    template.setDataModel(dto.getDataModel());
                    templateRepository.save(template);
                    log.info(CdrHelper.successFormatCdrLog("updateTemplate", template));
                    return dto;
                })
                .orElseThrow(() -> {
                    log.info(CdrHelper.errorFormatCdrLog("updateTemplate", code));
                    return new NotFoundException(code);
                });
    }

    @Override
    public Optional<TemplateDTO> findOne(Long id) {
        log.info(CdrHelper.infoFormatCdrLog("findOneTemplate", id));
        return templateRepository.findById(id)
                .map(TemplateDTO::fromEntity);
    }

    @Override
    public Optional<TemplateDTO> findOne(String code, Long eId) {
        log.info(CdrHelper.infoFormatCdrLog("findOneTemplate", String.format("{\"id\":%s,\"enterprise\": %d}", code, eId)));
        return Optional.of(templateRepository.findByCodeAndEntrepriseId(code, eId))
                .map(TemplateDTO::fromEntity);
    }

    @Override
    public List<TemplateDTO> findAll(Long enterpriseId) {
        log.info(CdrHelper.infoFormatCdrLog("findAllTemplate", String.format("{\"enterprise\": %d}", enterpriseId)));
        return templateRepository.findByEntrepriseId(enterpriseId)
                .stream()
                .map(TemplateDTO::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(Long id, Long enterpriseId) {
        log.info(CdrHelper.successFormatCdrLog("deleteTemplate", String.format("{\"id\":%d,\"enterprise\": %d}", id, enterpriseId)));
        templateRepository.deleteByEntrepriseIdAndId(enterpriseId, id);
    }

    @Override
    public TemplateFieldDTO saveField(TemplateFieldDTO dto, Long templateId) {
        TemplateField field = dto.toEntity();
        field.setTemplate(Template.builder().id(templateId).build());
        TemplateField saved = templateFieldRepository.save(field);
        log.info(CdrHelper.successFormatCdrLog("createTemplateField", saved));
        return TemplateFieldDTO.fromEntity(saved);
    }

    @Override
    public TemplateFieldDTO updateField(TemplateFieldDTO dto, Long templateId) throws NotFoundException {
        return templateFieldRepository.findById(dto.getId())
                .map(templateField -> {
                    templateField.setDefaultValue(dto.getDefaultValue());
                    templateField.setName(dto.getName());
                    templateField.setDescription(dto.getDescription());
                    templateFieldRepository.save(templateField);
                    log.info(CdrHelper.successFormatCdrLog("updateTemplateField", dto));
                    return dto;
                }).orElseThrow(() -> {
                    log.info(CdrHelper.successFormatCdrLog("updateTemplateField", dto));
                    return new NotFoundException(dto.getName());
                });
    }

    @Override
    public void removeField(Long id) {
        templateFieldRepository.deleteById(id);
        log.info(CdrHelper.successFormatCdrLog("deleteTemplateField", String.format("{\"id\":%d}", id)));
    }

    @Override
    public List<TemplateFieldDTO> findFieldByTemplateId(Long enterpriseId, Long templateId) {
        log.info(CdrHelper.infoFormatCdrLog("findTemplateFieldByTemplateId", String.format("{\"enterprise\": %d}", enterpriseId)));
        return templateFieldRepository.findByEntrepriseIdAndTemplate_Id(enterpriseId, templateId)
                .stream()
                .map(TemplateFieldDTO::fromEntity)
                .collect(Collectors.toList());
    }
}
