package com.nivekaa.mailingserver.service.impl;

import com.nivekaa.mailingserver.configs.MailProperties;
import com.nivekaa.mailingserver.model.Mail;
import com.nivekaa.mailingserver.rest.util.CdrHelper;
import com.nivekaa.mailingserver.service.IMailService;
import com.nivekaa.mailingserver.service.MailingConfigurationService;
import com.nivekaa.mailingserver.service.dto.MConfigDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.time.ZonedDateTime;
import java.util.Locale;
import java.util.Map;

import static com.nivekaa.mailingserver.Util._CDR_OPERATIONS;

/**
 * @author Nivekaa
 * Created 10/02/2020 at 23:46
 * Class com.nivekaa.mailingserver.service.impl.MailServiceImpl
 */


@Service
// @Profile("prod")
public class MailServiceImpl implements IMailService {
    private static final Logger log = LoggerFactory.getLogger(_CDR_OPERATIONS);
    private final JavaMailSender javaMailSender;
    private final MessageSource messageSource;
    // private final SpringTemplateEngine templateEngine;
    private final ITemplateEngine templateEngine;
    private final MailProperties emailConfig;
    private Context context = null;
    private final MailingConfigurationService configurationService;

    public MailServiceImpl(JavaMailSender javaMailSender, MessageSource messageSource, ITemplateEngine templateEngine, MailProperties emailConfig, MailingConfigurationService configurationService) {
        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
        this.emailConfig = emailConfig;
        this.configurationService = configurationService;
    }

    protected MimeMessageHelper preBuild(MimeMessage mimeMessage, Mail mail, boolean isMultipart, boolean asHTML) throws Throwable {
        final MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage,isMultipart, StandardCharsets.UTF_8.name());
        final String from = StringUtils.isEmpty(mail.getFrom())?emailConfig.getUsername():mail.getFrom();
        String[] tos = mail.getTo().split(",");

        MConfigDTO mConfigDTO = configurationService.findByEnterprise(mail.getEntrepriseId());
        String fromPersonal = mConfigDTO.getFromName();
        String fromEmail = mConfigDTO.getFromEmail();
        String footer = StringUtils.isEmpty(mConfigDTO.getFooterContent())
                ? "" : mConfigDTO.getFooterContent();
        String header = StringUtils.isEmpty(mConfigDTO.getHeaderContent())
                ? "" : mConfigDTO.getHeaderContent();
        String content = String.format("%s%s%s", header, mail.getContent(), footer);

        if (mail.getCc() != null && !mail.getCc().isEmpty()){
            String[] ccs = mail.getCc().split(",");
            messageHelper.setCc(ccs);
        }

        if (mail.getBcc() != null && !mail.getBcc().isEmpty()){
            String[] bccs = mail.getBcc().split(",");
            messageHelper.setBcc(bccs);
        }
        messageHelper.setSentDate(Date.valueOf(ZonedDateTime.now().toLocalDate()));
        // messageHelper
        messageHelper.setTo(tos);
        messageHelper.setFrom(fromEmail, fromPersonal);
        messageHelper.setSubject(mail.getSubject());
        messageHelper.setText(content,true);
        return  messageHelper;
    }

    @Override
    public void simple(Mail utilities, boolean asHTML) {
        final MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try{
            final MimeMessageHelper messageHelper = this.preBuild(mimeMessage,utilities,false,true);
            javaMailSender.send(mimeMessage);
            log.info(CdrHelper.successFormatCdrLog("sendSimpleMail", utilities));
        } catch (final Throwable e) {
            e.printStackTrace();
            log.info(CdrHelper.errorFormatCdrLog("sendSimpleMail", e.getMessage()));
        }
    }

    @Override
    public void simpleWithAttachments(Mail utilities, boolean asHTML, File[] files) {
        final MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try{
            mimeMessage.setContentLanguage(new String[]{"en","fr"});
            final MimeMessageHelper messageHelper = this.preBuild(mimeMessage,utilities,true,true);
            if (files!=null && files.length!=0){
                for (final File file : files) {
                    messageHelper.addAttachment(file.getName(), file);
                }
            }
            javaMailSender.send(mimeMessage);
            log.info(CdrHelper.successFormatCdrLog("sendSimpleMailWithAttachments", utilities));
        } catch (final Throwable e) {
            e.printStackTrace();
            log.info(CdrHelper.errorFormatCdrLog("sendSimpleMailWithAttachments", e.getMessage()));
        }
    }

    public void simpleWithAttachments(Mail utilities, boolean asHTML, InputStreamSource[] files, String[] fileName) {
        final MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try{
            final MimeMessageHelper messageHelper = this.preBuild(mimeMessage,utilities,true,true);
            if (files!=null && files.length!=0){
                int i=0;
                for (final InputStreamSource file : files) {
                    messageHelper.addAttachment(fileName[i], file);
                    i++;
                }
            }
            javaMailSender.send(mimeMessage);
            log.info(CdrHelper.successFormatCdrLog("sendSimpleMailWithAttachments", utilities));
        } catch (final Throwable e) {
            e.printStackTrace();
            log.info(CdrHelper.errorFormatCdrLog("sendSimpleMailWithAttachments", e.getMessage()));
        }
    }

    @Override
    public void withTemplate(Mail utilities, Map<String, Object> variables) {
        final MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try{
            if (utilities.getLangKey()!=null && !"".equals(utilities.getLangKey())) {
                context = new Context(Locale.forLanguageTag(utilities.getLangKey()));
            } else{
                context = new Context(Locale.FRANCE);
            }
            //pusher les variables utile dans le html ici
            if (variables!=null && !variables.isEmpty()){
                for (final Map.Entry<String,Object> entry: variables.entrySet()) {
                    context.setVariable(entry.getKey(),entry.getValue());
                }
            }
            context.setVariable("url","https://nivekaa.com");
            final String content = templateEngine.process(utilities.getContent(),context);
            utilities.setContent(content);
            javaMailSender.send(mimeMessage);
            log.info(CdrHelper.successFormatCdrLog("sendMailWithTemplate", utilities));
        } catch (Exception e) {
            e.printStackTrace();
            log.info(CdrHelper.errorFormatCdrLog("sendMailWithTemplate", e.getMessage()));
        }
    }

    @Override
    public void withTemplateAndAttachments(Mail utilities, Map<String, Object> variables, File[] files) {
        final MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try{
            if (utilities.getLangKey()!=null && !"".equals(utilities.getLangKey())) {
                context = new Context(Locale.forLanguageTag(utilities.getLangKey()));
            } else{
                context = new Context(Locale.FRANCE);
            }
            //pusher les variables utile dans le html ici
            if (variables!=null && !variables.isEmpty()){
                for (final Map.Entry<String,Object> entry: variables.entrySet()) {
                    context.setVariable(entry.getKey(),entry.getValue());
                }
            }
            context.setVariable("url","https://nivekaa.com");
            final String content = templateEngine.process(utilities.getContent(),context);
            utilities.setContent(content);
            final MimeMessageHelper messageHelper = this.preBuild(mimeMessage,utilities,true,true);
            //ajouter les pieces jointes
            if (files!=null && files.length!=0){
                for (final File file : files) {
                    messageHelper.addAttachment(file.getName(), file);
                }
            }
            javaMailSender.send(mimeMessage);
            log.info(CdrHelper.successFormatCdrLog("sendMailWithTemplateAndAttachments", utilities));
        } catch (final Throwable e) {
            e.printStackTrace();
            log.info(CdrHelper.errorFormatCdrLog("sendMailWithTemplateAndAttachments", e.getMessage()));
        }
    }

}
