package com.nivekaa.mailingserver.service;

import com.nivekaa.mailingserver.model.Mail;
import com.nivekaa.mailingserver.repositories.FileStoreRepository;
import com.nivekaa.mailingserver.repositories.MailEntityRepository;
import com.nivekaa.mailingserver.rest.util.CdrHelper;
import com.nivekaa.mailingserver.service.dto.MailDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.nivekaa.mailingserver.Util._CDR_OPERATIONS;

/**
 * @author nivekaa
 * Created 23/02/2020 at 20:03
 * Class com.nivekaa.mailingserver.service.MailEntityService
 */

@Service
public class MailEntityService {
    private final MailEntityRepository repository;
    private final FileStoreRepository fileStoreRepository;

    private static final Logger log = LoggerFactory.getLogger(_CDR_OPERATIONS);

    public MailEntityService(MailEntityRepository repository, FileStoreRepository fileStoreRepository) {
        this.repository = repository;
        this.fileStoreRepository = fileStoreRepository;
    }

    public MailDTO save(Mail mailDTO){
        Mail mail = repository.save(mailDTO);
        log.info(CdrHelper.successFormatCdrLog("saveMailDto", mailDTO));
        return MailDTO.builder()
                .entrepriseId(mail.getEntrepriseId())
                .content(mail.getContent())
                .dateTime(mail.getDateTime())
                .from(mail.getFrom())
                .to(mail.getTo())
                .langKey(mail.getLangKey())
                .id(mail.getId())
                .subject(mail.getSubject())
                .timeZone(mail.getTimeZone())
                .files(
                        fileStoreRepository.findAllByMail_IdAndEntrepriseId(mail.getId(), mail.getEntrepriseId())
                )
                .build();
    }

    public MailDTO getByEntreprise(Long id, Long entreprise){
        return Optional.of(repository.findByIdAndEntrepriseId(id,entreprise))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(mail -> {
                    log.info(CdrHelper.infoFormatCdrLog("getMailDtoByEntreprise", mail));
                            return MailDTO.builder()
                                    .entrepriseId(mail.getEntrepriseId())
                                    .content(mail.getContent())
                                    .dateTime(mail.getDateTime())
                                    .from(mail.getFrom())
                                    .to(mail.getTo())
                                    .langKey(mail.getLangKey())
                                    .id(mail.getId())
                                    .subject(mail.getSubject())
                                    .timeZone(mail.getTimeZone())
                                    .files(
                                            fileStoreRepository.findAllByMail_IdAndEntrepriseId(id, entreprise)
                                    )
                                    .build();
                        }
                ).get();
    }

    public List<MailDTO> getAllByEntreprise(Long entreprise){
        return repository.findAllByEntrepriseId(entreprise, Pageable.unpaged())
                .stream()
                .map(mail -> {
                    log.info(CdrHelper.infoFormatCdrLog("getAllMailDtoByEntreprise", entreprise));
                            return MailDTO.builder()
                                    .entrepriseId(mail.getEntrepriseId())
                                    .content(mail.getContent())
                                    .dateTime(mail.getDateTime())
                                    .from(mail.getFrom())
                                    .to(mail.getTo())
                                    .langKey(mail.getLangKey())
                                    .id(mail.getId())
                                    .subject(mail.getSubject())
                                    .timeZone(mail.getTimeZone())
                                    .files(
                                            fileStoreRepository.findAllByMail_IdAndEntrepriseId(mail.getId(), entreprise)
                                    ).build();
                        }
                ).collect(Collectors.toList());
    }

    public void deleteByEntreprise(Long id, Long entrepriseId){
        log.info(CdrHelper.successFormatCdrLog("deleteMailDtoByEntreprise", id));
        repository.deleteByIdAndEntrepriseId(id,entrepriseId);
    }
}
