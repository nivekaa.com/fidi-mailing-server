package com.nivekaa.mailingserver.service;

import com.nivekaa.mailingserver.configs.AppProperties;
import com.nivekaa.mailingserver.model.Template;
import com.nivekaa.mailingserver.repositories.TemplateFieldRepository;
import com.nivekaa.mailingserver.repositories.TemplateRepository;
import com.nivekaa.mailingserver.rest.error.NotFoundException;
import com.nivekaa.mailingserver.rest.util.FilesUtil;
import com.nivekaa.mailingserver.service.dto.TemplateFormatedDTO;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.StringTemplateResolver;

import java.util.Locale;
import java.util.Map;

/**
 * @author kevin
 * Created 15/10/2020 at 22:15
 * Class com.nivekaa.mailingserver.service.TemplateResolverService
 */

@Service
@RequiredArgsConstructor
public class TemplateResolverService {
    private final TemplateRepository templateRepository;
    private final AppProperties properties;
    private TemplateEngine templateEngine;

    public TemplateFormatedDTO getTemplateContentAsString(String codeOrId, Map<String, Object> dynamicAttibutesMap) throws NotFoundException {
        return getTemplateContentAsString(codeOrId, dynamicAttibutesMap, Locale.FRENCH.toString());
    }

    public TemplateFormatedDTO getTemplateContentAsString(String codeOrId, Map<String, Object> dynamicAttibutesMap, String TEMPLATE_LOCAL) throws NotFoundException {
        Template template;
        if (StringUtils.isNumeric(codeOrId)) {
            template = templateRepository.findById(Long.valueOf(codeOrId))
                    .orElseThrow(() -> new NotFoundException(codeOrId));
        } else {
            template = templateRepository.findByCode(codeOrId)
                    .orElseThrow(() -> new NotFoundException(codeOrId));
        }
        String htmlcontent;
        final Context ctx = new Context(new Locale(TEMPLATE_LOCAL));
        if (!CollectionUtils.isEmpty(template.getFields())) {
            template.getFields().forEach(field ->{
                ctx.setVariable(field.getName(), dynamicAttibutesMap.getOrDefault(field.getName(), ""));
            });
        }
        templateEngine = getTemplateEngine();
        String content = template.getContent()!=null && template.getContent().endsWith(".html")
                ? FilesUtil.readFileToString(template.getContent())
                : template.getContent();
        ctx.setVariable("baseUrl", properties.getBaseUrl());
        htmlcontent = templateEngine.process(content, ctx);
        //templateEngine.pro
        //System.err.println(template);
        return TemplateFormatedDTO.builder()
                .html(htmlcontent)
                .build();
    }

    private TemplateEngine getTemplateEngine() {
        if(null == templateEngine){
            templateEngine = new TemplateEngine();
            StringTemplateResolver templateResolver =new   StringTemplateResolver();
            templateResolver.setTemplateMode(TemplateMode.HTML);
            templateEngine.setTemplateResolver(templateResolver);
        }
        return templateEngine;
    }
}
