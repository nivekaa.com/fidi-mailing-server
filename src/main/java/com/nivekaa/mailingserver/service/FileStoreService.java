package com.nivekaa.mailingserver.service;

import com.nivekaa.mailingserver.model.FileStore;
import com.nivekaa.mailingserver.repositories.FileStoreRepository;
import com.nivekaa.mailingserver.rest.util.CdrHelper;
import com.nivekaa.mailingserver.service.dto.FileStoreDTO;
import com.nivekaa.mailingserver.service.dto.MailDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.nivekaa.mailingserver.Util._CDR_OPERATIONS;

/**
 * @author nivekaa
 * Created 23/02/2020 at 20:27
 * Class com.nivekaa.mailingserver.service.FileStoreService
 */

@Service
public class FileStoreService {
    private Logger log = LoggerFactory.getLogger(_CDR_OPERATIONS);
    private final FileStoreRepository repository;

    public FileStoreService(FileStoreRepository repository) {
        this.repository = repository;
    }

    public FileStoreDTO save(FileStore file){
        FileStore sfile = repository.save(file);
        log.info(CdrHelper.successFormatCdrLog("saveFileStore", file));
        return FileStoreDTO.builder()
                .id(sfile.getId())
                .location(sfile.getLocation())
                .filename(sfile.getFilename())
                .entrepriseId(sfile.getEntrepriseId())
                .fileType(sfile.getFileType())
                .build();
    }
}
