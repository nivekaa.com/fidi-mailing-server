package com.nivekaa.mailingserver.service;

import com.nivekaa.mailingserver.rest.error.NotFoundException;
import com.nivekaa.mailingserver.service.dto.TemplateDTO;
import com.nivekaa.mailingserver.service.dto.TemplateFieldDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author kevin
 * Created 10/10/2020 at 03:06
 * Class com.nivekaa.mailingserver.service.TemplateService
 */

@Service
public interface TemplateService {
    TemplateDTO save(TemplateDTO dto);
    TemplateDTO update(String code, TemplateDTO dto) throws NotFoundException;
    Optional<TemplateDTO> findOne(Long id);
    Optional<TemplateDTO> findOne(String code, Long eId);
    List<TemplateDTO> findAll(Long enterpriseId);
    void delete(Long id, Long enterpriseId);
    // field
    TemplateFieldDTO saveField(TemplateFieldDTO dto, Long templateId);
    TemplateFieldDTO updateField(TemplateFieldDTO dto, Long templateId) throws NotFoundException;
    void removeField(Long id);
    List<TemplateFieldDTO> findFieldByTemplateId(Long enterpriseId, Long templateId);
}
