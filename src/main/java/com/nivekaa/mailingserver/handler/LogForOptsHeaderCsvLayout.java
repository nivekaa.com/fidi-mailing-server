package com.nivekaa.mailingserver.handler;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * @author kevin
 * Created 10/10/2020 at 18:38
 * Class com.nivekaa.mailingserver.handler.LogForOptsHeaderCsvLayout
 */

public class LogForOptsHeaderCsvLayout/* extends PatternLayout*/ {
   /* private String header;

    public void setHeader(String header) {
        this.header = header;
    }

    @Override
    public String getFileHeader() {
        if (alreadyContainsHeader()) {
            return "";
        } else {
            return header;
        }
    }

    private boolean alreadyContainsHeader() {
        try(BufferedReader br = new BufferedReader(new FileReader("logs/log-operations.csv"))) {
            String line = null;
            while ((line = br.readLine()) != null) {
                if (line.contains(header)) {
                    return true;
                } else {
                    break;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
        return false;
    }*/
}
