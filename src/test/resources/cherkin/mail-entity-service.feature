Feature: Mail Service
  #Ici on interagit avec la base de données
  Scenario: Fetch list
    When je veux la liste des mails
      And l'identifiant de l'entreprise est 1
    Then la liste est non vide, la liste contient 3 elements, le destinataire de l'elemnent 2 est to2@test.com
  Scenario: Fetch one
    When je veux un mail dont l'identifiant ID est 1, l'Id de l'entreprise est 1
    Then l'email en question doit être not null et l'expediteur doit etre from@test.com et destinateur to@test.com
  Scenario: Add new
    Given preparer l'enregistrement d'un mail
    When quand je fournis les infos: to=to@test.com from=from@test.com subject=BDD test content=content test
    Then Enregistrer. le resultat doit etre non null et BDD test comme object du mail nouvellement registré