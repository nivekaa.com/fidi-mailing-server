package com.nivekaa.mailingserver.rest.util;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author kevin
 * Created 10/10/2020 at 00:01
 * Class com.nivekaa.mailingserver.rest.util.CdrHelperTest
 */

public class CdrHelperTest {

    @Test
    public void test_formatCdrLog() {
        String values = CdrHelper.formatCdrLog("abc","xyz","123");
        assertThat(values).isNotNull();
        assertThat(values).isEqualTo("abc;xyz;123");
    }
}