package com.nivekaa.mailingserver.rest.util;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author nivekaa
 * Created 15/08/2020 at 19:44
 * Class com.nivekaa.mailingserver.rest.util.TestParser
 */

public class TestParserUtil {
    public static String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }
    public static  <T> T mapFromJson(String json, Class<T> clazz) throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, clazz);
    }


    public static MultipartFile getMpFile(int size) {
        return new MultipartFile() {
            @Override
            public String getName() {
                return "file.txt";
            }

            @Override
            public String getOriginalFilename() {
                return "file.txt";
            }

            @Override
            public String getContentType() {
                return "plain/text";
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return size*1024*1024;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return "je suis un mock".getBytes();
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public void transferTo(File file) throws IOException, IllegalStateException {

            }
        };
    }
}
