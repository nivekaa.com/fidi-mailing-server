package com.nivekaa.mailingserver.rest.util;

import com.nivekaa.mailingserver.configs.ConfigMailingConfiguration;
import io.cucumber.java8.Fi;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


/**
 * @author nivekaa
 * Created 14/08/2020 at 10:35
 * Class com.nivekaa.mailingserver.rest.util.FilesUtilTest
 */

public class FilesUtilTest {

    @Mock
    private ConfigMailingConfiguration configuration = new ConfigMailingConfiguration();

    @Before
    public void setUp() throws Exception {
        ReflectionTestUtils.setField(configuration, "maxSizeFilesUpload", 2);
        ReflectionTestUtils.setField(configuration, "maxAttachments", 2);
    }

    @Test
    public void test_countFiles() {
        MultipartFile[] files = new MultipartFile[2];
        files[0] = (MultipartFile)null;
        files[1] = (MultipartFile)null;
        assertThat(FilesUtil.countFiles(files)).isNotNull();
        assertThat(FilesUtil.countFiles(files)).isNotZero();
        assertThat(FilesUtil.countFiles(files)).isEqualTo(2);
    }

    @Test
    public void test_reachedMaxAttachments_is_attempted() {
        MultipartFile[] files = new MultipartFile[3];
        files[0] = (MultipartFile)null;
        files[1] = (MultipartFile)null;
        files[2] = (MultipartFile)null;
        boolean o = FilesUtil.reachedMaxAttachments(files, configuration);
        assertThat(o).isTrue();
    }

    @Test
    public void test_reachedMaxAttachments_is_not_attempted() {
        MultipartFile[] files = new MultipartFile[1];
        files[0] = (MultipartFile)null;
        boolean o = FilesUtil.reachedMaxAttachments(files, configuration);
        assertThat(o).isFalse();
    }

    @Test
    public void test_allFileSize() {
        MultipartFile[] files = new MultipartFile[2];
        files[0] = getMpFile(5*1024*1024);
        files[1] = getMpFile(5*1024*1024);
        double size = FilesUtil.allFileSize(files);
        assertThat(size).isNotNaN();
        assertThat(size).isNotZero();
        assertThat(size).isEqualTo(10);
    }

    @Test
    public void test_maxFileSizeReached_is_attempted() {
        MultipartFile[] files = new MultipartFile[3];
        files[0] = getMpFile(5*1024*1024);
        files[1] = getMpFile();
        files[2] = getMpFile();
        boolean o = FilesUtil.maxFileSizeReached(files, configuration);
        assertThat(o).isTrue();
    }

    @Test
    public void test_maxFileSizeReached_is_not_attempted() {
        MultipartFile[] files = new MultipartFile[1];
        files[0] = getMpFile(5*1024);
        boolean o = FilesUtil.maxFileSizeReached(files, configuration);
        assertThat(o).isFalse();
    }

    private MultipartFile getMpFile() {
        return new MultipartFile() {
            @Override
            public String getName() {
                return "file.txt";
            }

            @Override
            public String getOriginalFilename() {
                return "file.txt";
            }

            @Override
            public String getContentType() {
                return "plain/text";
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return 12;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return "je suis un mock".getBytes();
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public void transferTo(File file) throws IOException, IllegalStateException {

            }
        };
    }
    private MultipartFile getMpFile(int size) {
        return new MultipartFile() {
            @Override
            public String getName() {
                return "file.txt";
            }

            @Override
            public String getOriginalFilename() {
                return "file.txt";
            }

            @Override
            public String getContentType() {
                return "plain/text";
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return size;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return "je suis un mock".getBytes();
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public void transferTo(File file) throws IOException, IllegalStateException {

            }
        };
    }

    @Test
    public void test_readFileToString() {
        String content = FilesUtil.readFileToString("files/test.txt");
        assertThat(content).isNotNull();
        assertThat(content.length()).isGreaterThan(0);
    }

    @Test
    public void test_readFileToString_when_unreachable_file() {
        assertThrows(UncheckedIOException.class, ()-> {
            FilesUtil.readFileToString("fffffiles/test.txt");
        });
    }

    @Test
    public void test_asString() {
        Resource resource = new DefaultResourceLoader().getResource("files/test.txt");
        String content = FilesUtil.asString(resource);
        assertThat(content).isNotNull();
        assertThat(content.length()).isGreaterThan(0);
    }

    @Test
    public void test_asString_when_throwable() {
        assertThrows(UncheckedIOException.class, ()-> {
            FilesUtil.readFileToString("fffffiles/test.txt");
        });
    }
}