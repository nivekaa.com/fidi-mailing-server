package com.nivekaa.mailingserver.rest.controllers;

import com.nivekaa.mailingserver.MailingServerApplication;
import com.nivekaa.mailingserver.Util;
import com.nivekaa.mailingserver.rest.error.ApiException;
import com.nivekaa.mailingserver.service.MailingConfigurationService;
import com.nivekaa.mailingserver.service.dto.MConfigDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static com.nivekaa.mailingserver.rest.util.TestParserUtil.mapFromJson;
import static com.nivekaa.mailingserver.rest.util.TestParserUtil.mapToJson;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author nivekaa
 * Created 04/04/2020 at 15:33
 * Class com.nivekaa.mailingserver.rest.controllers.ConfigurationControllerTest
 */

@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest(classes = MailingServerApplication.class)
@WebAppConfiguration
public class ConfigurationControllerTest{
    @InjectMocks
    private ConfigurationController controller;
    @Mock
    MailingConfigurationService service;

    protected MockMvc mvc;
    @Before
    public void setUp() {
        controller = new ConfigurationController(service);
        mvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void sould_get_config_when_id_is_given() throws Throwable {
        String uri = "/config/find/1";
        when(service.findByEnterprise(1L))
                .thenReturn(getMConfigMock().get(0));

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .param("entrepriseId","1")
                .header(Util.HEADER_USER_ID_KEY,"1")
                .header(Util.HEADER_ENTERPRISE_ID_KEY,"1")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertThat(status).isEqualTo(200);
        String content = mvcResult.getResponse().getContentAsString();
        assertThat(content).isNotNull();
        MConfigDTO mConfigDTO = mapFromJson(content, MConfigDTO.class);
        assertThat(mConfigDTO).isNotNull();
        assertThat(mConfigDTO).hasFieldOrProperty("fromName");
        assertThat(mConfigDTO.getFromName()).isEqualTo("NAME_1");

        //assertTrue(productlist.length > 0);
    }
    @Test(expected = Exception.class)
    public void should_get_config_non_existing() throws Throwable {
        String uri = "/config/find/{entrepriseId}";
        when(service.findByEnterprise(1L))
                .thenThrow(Exception.class);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .param("entrepriseId","1")
                .header(Util.HEADER_USER_ID_KEY,"1")
                .header(Util.HEADER_ENTERPRISE_ID_KEY,"1")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andDo(print())
                .andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertThat(status).isEqualTo(400);
        String content = mvcResult.getResponse().getContentAsString();
        assertThat(content).isNotNull();
        assertThat(content).isEqualTo("Not found");
    }

    @Test
    public void should_save_new_config_successfully() throws Exception {
        // given
        MConfigDTO toSave = getMConfigMock().get(1);
        toSave.setId(null);
        toSave.setEntrepriseId(1L);
        String uri = "/config/save";
        String body = mapToJson(toSave);
        // When
        when(service.save(toSave))
                .thenReturn(getMConfigMock().get(1));
        // Then
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .header(Util.HEADER_USER_ID_KEY,"1")
                .header(Util.HEADER_ENTERPRISE_ID_KEY,"1")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(body))
                .andExpect(status().is2xxSuccessful())
                .andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        int status = mvcResult.getResponse().getStatus();
        assertThat(status).isEqualTo(200);
        assertThat(content).isNotNull();
        MConfigDTO mConfigDTO = mapFromJson(content, MConfigDTO.class);
        assertThat(mConfigDTO).isNotNull();
        assertThat(mConfigDTO).hasFieldOrProperty("fromName");
        assertThat(mConfigDTO.getFromName()).isEqualTo("NAME_2");
    }

    @Test
    public void should_save_new_config_and_return_error_400_badRequest_its_contentType_wrong() throws Exception {
        // given
        MConfigDTO toSave = getMConfigMock().get(1);
        toSave.setId(null);
        toSave.setFromName(null);
        String uri = "/config/save";
        String body = mapToJson(toSave);
        // When
        when(service.save(toSave))
                .thenReturn(getMConfigMock().get(1));
        // Then
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .header(Util.HEADER_USER_ID_KEY,"1")
                .header(Util.HEADER_ENTERPRISE_ID_KEY,"1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(result -> assertThat(result.getResolvedException() instanceof ApiException.BadArgumentsException).isTrue())
                .andExpect(result -> assertThat(result.getResolvedException().getMessage()).contains("This fields is required","from name"));
    }

    @Test
    public void should_save_new_config_and_return_error_400_mediaType_wrong() throws Exception {
        // given
        MConfigDTO toSave = getMConfigMock().get(1);
        toSave.setId(null);
        toSave.setEntrepriseId(1L);
        String uri = "/config/save";
        String body = mapToJson(toSave);
        // When
        when(service.save(toSave))
                .thenReturn(getMConfigMock().get(1));
        // Then
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .header(Util.HEADER_USER_ID_KEY,"1")
                .header(Util.HEADER_ENTERPRISE_ID_KEY,"1")
                .content(body))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertThat(status).isEqualTo(415);
    }

    @Test
    public void should_update_config_existing_data() throws Exception {
        // given
        MConfigDTO toSave = getMConfigMock().get(1);
        toSave.setFromName("NIVEKAA");
        toSave.setFromEmail("nivekaa@test.com");
        toSave.setEntrepriseId(1L);
        String uri = "/config/update";
        String body = mapToJson(toSave);
        MConfigDTO updated = getMConfigMock().get(1);
        updated.setFromName("NIVEKAA");
        updated.setFromEmail("nivekaa@test.com");
        // When
        when(service.save(toSave))
                .thenReturn(updated);
        // Then
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .header(Util.HEADER_USER_ID_KEY,"1")
                .header(Util.HEADER_ENTERPRISE_ID_KEY,"1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json("{\"fromName\": \"NIVEKAA\"}"))
                .andExpect(content().json("{\"fromEmail\": \"nivekaa@test.com\"}"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

    }

    private List<MConfigDTO> getMConfigMock() {
        return Arrays.asList(
                MConfigDTO.builder()
                        .id(1L)
                        .fromEmail("my1@test.com")
                        .headerContent("content h_1")
                        .fromName("NAME_1")
                        .build(),
                MConfigDTO.builder()
                        .id(2L)
                        .fromEmail("my1@test.com")
                        .headerContent("content h_2")
                        .fromName("NAME_2")
                        .build(),
                MConfigDTO.builder()
                        .id(3L)
                        .fromEmail("my3@test.com")
                        .headerContent("content h_3")
                        .fromName("NAME_3")
                        .build()
        );
    }

}