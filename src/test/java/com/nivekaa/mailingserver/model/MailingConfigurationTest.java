package com.nivekaa.mailingserver.model;

import com.nivekaa.mailingserver.service.dto.MConfigDTO;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * @author nivekaa
 * Created 14/08/2020 at 09:51
 * Class com.nivekaa.mailingserver.model.MailingConfigurationTest
 */

public class MailingConfigurationTest {

    @Test
    public void test_toDto() {
        MailingConfiguration entity = MailingConfiguration.builder()
                .fromName("NAME")
                .fromEmail("my@gmail.com")
                .build();

        // Then
        assertThat(entity.toDto()).isInstanceOf(MConfigDTO.class);
        MConfigDTO dto = entity.toDto();
        assertThat(dto).isNotNull();
        assertThat(dto.getFromEmail()).isEqualTo(entity.getFromEmail());
        assertThat(dto.getFromName()).isEqualTo(entity.getFromName());
    }
}