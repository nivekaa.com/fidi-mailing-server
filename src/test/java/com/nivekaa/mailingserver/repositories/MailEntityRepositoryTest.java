package com.nivekaa.mailingserver.repositories;

import com.mmnaseri.utils.spring.data.dsl.factory.RepositoryFactoryBuilder;
import com.mmnaseri.utils.spring.data.dsl.mock.RepositoryMockBuilder;
import com.mmnaseri.utils.spring.data.proxy.RepositoryFactoryConfiguration;
import com.nivekaa.mailingserver.model.Mail;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author nivekaa
 * Created 14/08/2020 at 12:23
 * Class com.nivekaa.mailingserver.repositories.MailEntityRepositoryTest
 */

public class MailEntityRepositoryTest {

    private MailEntityRepository repository;

    @Before
    public void setUp() throws Exception {
        RepositoryFactoryConfiguration configuration = RepositoryFactoryBuilder.builder()
                .adaptResultsUsing(new TestAdapterConfiguration())
                .configure();
        repository = new RepositoryMockBuilder().useConfiguration(configuration).mock(MailEntityRepository.class);
        repository.save(Mail.builder().id(1L).entrepriseId(1L).build());
    }

    @Test
    public void should_get_mail_when_id_and_enterprise_id_is_given() {
        // Given
        Long id=1L, eId=1L;
        // Then
        Optional<Mail> optional = repository.findByIdAndEntrepriseId(id, eId);
        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isTrue();
        assertThat(optional.get().getId()).isEqualTo(1L);
    }

    @Test
    public void should_get_all_mail_whe_enterprise_id_is_given() {
        // Given
        Long id=1L;
        // Then
        Page<Mail> optional = repository.findAllByEntrepriseId(id, PageRequest.of(0, 35));
        assertThat(optional).isNotNull();
        assertThat(optional.isEmpty()).isFalse();
        assertThat(optional.getTotalPages()).isEqualTo(1);
        assertThat(optional.getContent().size()).isEqualTo(1);
    }

    @Test
    public void should_delete_mail_when_id_and_enterprise_id_is_given() {
        // Given
        Long id=1L, eId=1L;
        // Then
        repository.deleteByIdAndEntrepriseId(id,eId);
        Optional<Mail> optional = repository.findByIdAndEntrepriseId(id, eId);
        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isFalse();
    }
}