package com.nivekaa.mailingserver.repositories;

import com.mmnaseri.utils.spring.data.domain.Invocation;
import com.mmnaseri.utils.spring.data.proxy.ResultAdapter;
import org.springframework.data.domain.PageImpl;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author nivekaa
 * Created 14/08/2020 at 13:17
 * Class com.nivekaa.mailingserver.repositories.TestAdapterConfiguration
 */

public class TestAdapterConfiguration implements ResultAdapter<Object> {

    private String[] SETABLE_METHODS = new String[]{"findAllByMail_IdAndEntrepriseId",
            "findByMail_IdAndEntrepriseId"};
    private String[] LISTABLE_METHODS = new String[]{};

    @Override
    public int compareTo(ResultAdapter resultAdapter) {
        return resultAdapter.getPriority();
    }

    @Override
    public boolean accepts(Invocation invocation, Object o) {
        //return !(o instanceof LinkedList);
        return true;
    }

    @Override
    public Object adapt(Invocation invocation, Object o) {
        if (invocation.getMethod().getName().startsWith("findBy")) {
            LinkedList elements = (LinkedList)o;
            if (elements.size()==0)
                return Optional.empty();
            return Optional.of((elements).get(0));
        }
        if (invocation.getMethod().getName().startsWith("findAll")) {
            try {
                System.out.println("---------------------------------");
                System.out.println(o);
                System.out.println("---------------------------------");
                LinkedList linkedList = (LinkedList)o;
                if (Arrays.asList(SETABLE_METHODS).contains(invocation.getMethod().getName())){
                    return linkedList.stream().collect(Collectors.toSet());
                } else if (Arrays.asList(LISTABLE_METHODS).contains(invocation.getMethod().getName())) {
                    return linkedList.stream().collect(Collectors.toList());
                }

                return new PageImpl<Object>(linkedList);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

        }
        return o;
    }

    @Override
    public int getPriority() {
        return 1;
    }
}
