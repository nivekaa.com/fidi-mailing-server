package com.nivekaa.mailingserver.repositories;

import com.mmnaseri.utils.spring.data.dsl.factory.RepositoryFactoryBuilder;
import com.mmnaseri.utils.spring.data.dsl.mock.RepositoryMockBuilder;
import com.mmnaseri.utils.spring.data.proxy.RepositoryFactoryConfiguration;
import com.nivekaa.mailingserver.model.MailingConfiguration;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author nivekaa
 * Created 14/08/2020 at 22:51
 * Class com.nivekaa.mailingserver.repositories.MailingConfigurationRepositoryTest
 */

public class MailingConfigurationRepositoryTest {

    private MailingConfigurationRepository repository;

    @Before
    public void setUp() throws Exception {
        RepositoryFactoryConfiguration configuration = RepositoryFactoryBuilder.builder()
                .adaptResultsUsing(new TestAdapterConfiguration())
                .configure();
        repository = new RepositoryMockBuilder().useConfiguration(configuration)
                .mock(MailingConfigurationRepository.class);
        repository.save(MailingConfiguration.builder()
                .id(1L)
                .entrepriseId(1L)
                .fromEmail("my@maul.com")
                .build());
        repository.save(MailingConfiguration.builder()
                .id(2L)
                .entrepriseId(1L)
                .fromEmail("your@maul.com")
                .build());
    }

    @Test
    public void test_findByEntrepriseId() {
        // Given
        Long id = 1L;
        // Then
        Optional<MailingConfiguration> optional = repository.findByEntrepriseId(id);
        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isTrue();
        assertThat(optional.get().getFromEmail()).isEqualTo("my@maul.com");
    }

    @Test
    public void findByIdAndEntrepriseId() {
        // Given
        Long id = 2L, eid = 1L;
        // Then
        Optional<MailingConfiguration> optional = repository.findByIdAndEntrepriseId(id, eid);
        assertThat(optional).isNotNull();
        assertThat(optional.isPresent()).isTrue();
        assertThat(optional.get().getFromEmail()).isEqualTo("your@maul.com");
    }
}