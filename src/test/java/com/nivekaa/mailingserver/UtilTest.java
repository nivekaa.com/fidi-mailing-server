package com.nivekaa.mailingserver;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author nivekaa
 * Created 14/08/2020 at 12:04
 * Class com.nivekaa.mailingserver.UtilTest
 */

public class UtilTest {

    @Test
    public void test_format_date() {
        assertThat(Util.DATE_TIME_FORMATTER.toString()).isNotNull();
        LocalDateTime localDateTime = LocalDateTime.of(2009,2,18,4,5,6, 28);
        assertThat(Util.DATE_TIME_FORMATTER.format(localDateTime)).isEqualTo("2009-02-18 04:05:06");
    }

    @Test
    public void test_constants() {
        assertThat(Util.HEADER_ENTERPRISE_ID_KEY).isEqualTo("X-ENTERPRISE-ID");
        assertThat(Util.HEADER_USER_ID_KEY).isEqualTo("X-USER-ID");
    }
}