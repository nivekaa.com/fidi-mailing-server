package com.nivekaa.mailingserver;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.spring.CucumberContextConfiguration;
import org.junit.runner.RunWith;

/**
 * @author nivekaa
 * Created 13/08/2020 at 14:16
 * Class com.nivekaa.mailingserver.RunBDDTest
 */

@RunWith(Cucumber.class)
@CucumberContextConfiguration
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber.html"},
        features = "classpath:cherkin/")
public class RunBDDTest {
}
