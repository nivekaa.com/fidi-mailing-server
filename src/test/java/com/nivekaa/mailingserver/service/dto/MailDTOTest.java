package com.nivekaa.mailingserver.service.dto;

import com.nivekaa.mailingserver.model.Mail;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author nivekaa
 * Created 14/08/2020 at 09:30
 * Class com.nivekaa.mailingserver.service.dto.MailDTOTest
 */

public class MailDTOTest {

    @Test
    public void shoud_build_entity_when_dto_is_given() {
        // Given
        MailDTO dto = MailDTO.builder()
                .content("content")
                .to("to@test.com")
                .from("from@test.com")
                .langKey("en")
                .build();
        // When
        // Then
        assertThat(dto.toEntity()).isInstanceOf(Mail.class);
        Mail entity = dto.toEntity();
        assertThat(entity.getFrom()).isEqualTo(dto.getFrom());
        assertThat(entity.getTo()).isEqualTo(dto.getTo());
        assertThat(entity.getContent()).isEqualTo(dto.getContent());
        assertThat(entity.getLangKey()).isEqualTo(dto.getLangKey());
    }
}