package com.nivekaa.mailingserver.service.dto;

import com.nivekaa.mailingserver.model.Template;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author kevin
 * Created 10/10/2020 at 02:56
 * Class com.nivekaa.mailingserver.service.dto.TemplateDTOTest
 */

public class TemplateDTOTest {

    @Test
    public void test_toEntity() {
        TemplateDTO dto = TemplateDTO.builder()
                .entrepriseId(1L)
                .content("CONTENT")
                .dataModel("com.nivekaa.ms_hr.model.User")
                .code("TEMPLATE-HR-001")
                .build();
        Template entity = dto.toEntity();
        assertThat(entity).isNotNull();
        assertThat(entity.getCode()).isEqualTo("TEMPLATE-HR-001");
        assertThat(entity.getContent()).isEqualTo("CONTENT");
        assertThat(entity.getDataModel()).isEqualTo("com.nivekaa.ms_hr.model.User");
        assertThat(entity.getEntrepriseId()).isEqualTo(1L);
    }

    @Test
    public void test_fromEntity() {
        Template entity = Template.builder()
                .entrepriseId(1L)
                .content("CONTENT")
                .dataModel("com.nivekaa.ms_hr.model.User")
                .code("TEMPLATE-HR-001")
                .build();

        TemplateDTO dto = TemplateDTO.fromEntity(entity);
        assertThat(dto).isNotNull();
        assertThat(dto.getCode()).isEqualTo("TEMPLATE-HR-001");
        assertThat(dto.getContent()).isEqualTo("CONTENT");
        assertThat(dto.getDataModel()).isEqualTo("com.nivekaa.ms_hr.model.User");
        assertThat(dto.getEntrepriseId()).isEqualTo(1L);
    }
}