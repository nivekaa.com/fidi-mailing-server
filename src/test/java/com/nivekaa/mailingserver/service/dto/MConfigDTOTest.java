package com.nivekaa.mailingserver.service.dto;

import com.nivekaa.mailingserver.model.MailingConfiguration;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author nivekaa
 * Created 14/08/2020 at 09:38
 * Class com.nivekaa.mailingserver.service.dto.MConfigDTOTest
 */

public class MConfigDTOTest {

    @Test
    public void should_build_entity_when_dto_is_given() {
        //Given
        MConfigDTO dto = MConfigDTO.builder()
                .fromName("NAME")
                .fromEmail("my@test.com")
                .headerContent("HEADER")
                .build();
        // When
        // Then
        assertThat(dto.toEntity()).isInstanceOf(MailingConfiguration.class);
        MailingConfiguration entity = dto.toEntity();
        assertThat(entity).isNotNull();
        assertThat(entity.getFromEmail()).isEqualTo(dto.getFromEmail());
        assertThat(entity.getFromName()).isEqualTo(dto.getFromName());
        assertThat(entity.getHeaderContent()).isEqualTo(dto.getHeaderContent());
        assertThat(entity.getEntrepriseId()).isEqualTo(dto.getEntrepriseId());
    }
}