package com.nivekaa.mailingserver.service.dto;

import com.nivekaa.mailingserver.model.Mail;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

/**
 * @author nivekaa
 * Created 14/08/2020 at 09:45
 * Class com.nivekaa.mailingserver.service.dto.SimpleMailDTOTest
 */

public class SimpleMailDTOTest {

    @Test
    public void toEntity() {
        // Given
        SimpleMailDTO dto = SimpleMailDTO.builder()
                .content("content")
                .to("to@test.com")
                .from("from@test.com")
                .cc("cc@test.com")
                .build();
        // When
        // Then
        assertThat(dto.toEntity()).isInstanceOf(Mail.class);
        Mail entity = dto.toEntity();
        assertThat(entity.getFrom()).isEqualTo(dto.getFrom());
        assertThat(entity.getTo()).isEqualTo(dto.getTo());
        assertThat(entity.getContent()).isEqualTo(dto.getContent());
        assertThat(entity.getCc()).isEqualTo(dto.getCc());
    }
}