package com.nivekaa.mailingserver.service.dto;

import com.nivekaa.mailingserver.model.TemplateField;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author kevin
 * Created 13/10/2020 at 02:43
 * Class com.nivekaa.mailingserver.service.dto.TemplateFieldDTOTest
 */

public class TemplateFieldDTOTest {

    @Test
    public void toEntity() {
        TemplateFieldDTO dto = TemplateFieldDTO.builder()
                .entrepriseId(1L)
                .name("name")
                .description("desc")
                .build();
        TemplateField entity = dto.toEntity();
        assertThat(entity).isNotNull();
        assertThat(entity.getDescription()).isEqualTo("desc");
        assertThat(entity.getName()).isEqualTo("name");
        assertThat(entity.getEntrepriseId()).isEqualTo(1L);
    }

    @Test
    public void fromEntity() {
        TemplateField entity = TemplateField.builder()
                .entrepriseId(1L)
                .name("name")
                .description("desc")
                .build();

        TemplateFieldDTO dto = TemplateFieldDTO.fromEntity(entity);
        assertThat(dto).isNotNull();
        assertThat(dto.getName()).isEqualTo("name");
        assertThat(dto.getDescription()).isEqualTo("desc");
        assertThat(dto.getEntrepriseId()).isEqualTo(1L);
    }
}