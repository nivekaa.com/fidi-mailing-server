package com.nivekaa.mailingserver.service.impl;

import com.nivekaa.mailingserver.configs.MailProperties;
import com.nivekaa.mailingserver.model.Mail;
import com.nivekaa.mailingserver.service.IMailService;
import com.nivekaa.mailingserver.service.MailingConfigurationService;
import com.nivekaa.mailingserver.service.dto.MConfigDTO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.MessageSource;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import java.io.*;
import java.util.HashMap;
import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author nivekaa
 * Created 13/08/2020 at 18:22
 * Class com.nivekaa.mailingserver.service.impl.MailServiceImplTest
 */

public class MailServiceImplTest {
    private IMailService iMailService;
    private Mail UTILITIES = Mail.builder()
            .id(1L).to("to@test.com").from("from@test.com").content("test content")
            .entrepriseId(1L).subject("BDD test").cc("cc@test.com").noFooter(false).build();

    @MockBean
    private MailServiceImpl mailService;
    @MockBean
    private JavaMailSender javaMailSender;
    @MockBean
    private MessageSource messageSource;
    private ITemplateEngine templateEngine;
    @MockBean
    private MailProperties emailConfig;
    @MockBean
    private MailingConfigurationService mailingConfigurationService;
    private MimeMessage mimeMessage;

    @Before
    public void setUp() throws Throwable {
        MockitoAnnotations.initMocks(this);
        iMailService = mock(IMailService.class);
        javaMailSender = mock(JavaMailSender.class);
        messageSource = mock(MessageSource.class);
        templateEngine = mock(ITemplateEngine.class);
        // emailConfig = mock(Mai.class);
        mailingConfigurationService = mock(MailingConfigurationService.class);
        when(mailingConfigurationService.findByEnterprise(UTILITIES.getEntrepriseId()))
                .thenReturn(MConfigDTO.builder().entrepriseId(UTILITIES.getEntrepriseId())
                        .headerContent("header mail")
                        .fromEmail("e@test.com")
                        .fromName("E-TDD")
                        .build());
        mimeMessage = new  MimeMessage((Session)null);
        mailService = new MailServiceImpl(javaMailSender, messageSource, templateEngine, emailConfig, mailingConfigurationService);
    }

    @Test
    public void should_send_simple_mail_with_mailPojo_given() {
        // when
        when(javaMailSender.createMimeMessage())
                .thenReturn(mimeMessage);
        // then
        mailService.simple(UTILITIES, true);
        verify(javaMailSender).createMimeMessage();
        verify(javaMailSender).send(mimeMessage);
    }

    @Test
    public void should_send_simple_mail_with_mailPojo_given_and_multiple_destionation() throws Throwable {
        // Given
        UTILITIES.setTo("to.1@gmail.com,to.2@gmail.com,to.3@gmail.com");
        // when
        when(javaMailSender.createMimeMessage())
                .thenReturn(mimeMessage);
        // then

        MimeMessageHelper mimeMessageHelper = mailService.preBuild(mimeMessage, UTILITIES, true, true);
        assertThat(mimeMessageHelper).isNotNull();
        assertThat(mimeMessageHelper.getMimeMessage()).isNotNull();
        assertThat(mimeMessageHelper.getMimeMessage().getRecipients(Message.RecipientType.TO)).isNotNull();
        assertThat(mimeMessageHelper.getMimeMessage().getRecipients(Message.RecipientType.TO).length).isEqualTo(3);
        assertThat(mimeMessageHelper.getMimeMessage().getRecipients(Message.RecipientType.TO)[0].toString()).isEqualTo("to.1@gmail.com");
        assertThat(mimeMessageHelper.getMimeMessage().getRecipients(Message.RecipientType.TO)[1].toString()).isEqualTo("to.2@gmail.com");
        assertThat(mimeMessageHelper.getMimeMessage().getRecipients(Message.RecipientType.TO)[2].toString()).isEqualTo("to.3@gmail.com");
    }

    @Test
    public void should_send_simple_mail_with_mailPojo_given_and_cc_exist_and_multiple() throws Throwable {
        // Given
        UTILITIES.setCc("cc.1@gmail.com,cc.2@gmail.com");
        // when
        when(javaMailSender.createMimeMessage())
                .thenReturn(mimeMessage);
        // then

        MimeMessageHelper mimeMessageHelper = mailService.preBuild(mimeMessage, UTILITIES, true, true);
        assertThat(mimeMessageHelper).isNotNull();
        assertThat(mimeMessageHelper.getMimeMessage()).isNotNull();
        assertThat(mimeMessageHelper.getMimeMessage().getRecipients(Message.RecipientType.CC)).isNotNull();
        assertThat(mimeMessageHelper.getMimeMessage().getRecipients(Message.RecipientType.CC).length).isEqualTo(2);
        assertThat(mimeMessageHelper.getMimeMessage().getRecipients(Message.RecipientType.CC)[0].toString()).isEqualTo("cc.1@gmail.com");
        assertThat(mimeMessageHelper.getMimeMessage().getRecipients(Message.RecipientType.CC)[1].toString()).isEqualTo("cc.2@gmail.com");
    }

    @Test
    public void should_send_simple_mail_with_mailPojo_given_and_bcc_exist_and_single() throws Throwable {
        // Given
        UTILITIES.setBcc("bcc.1@gmail.com");
        // when
        when(javaMailSender.createMimeMessage())
                .thenReturn(mimeMessage);
        // then

        MimeMessageHelper mimeMessageHelper = mailService.preBuild(mimeMessage, UTILITIES, true, true);
        assertThat(mimeMessageHelper).isNotNull();
        assertThat(mimeMessageHelper.getMimeMessage()).isNotNull();
        assertThat(mimeMessageHelper.getMimeMessage().getRecipients(Message.RecipientType.BCC)).isNotNull();
        assertThat(mimeMessageHelper.getMimeMessage().getRecipients(Message.RecipientType.BCC).length).isEqualTo(1);
        assertThat(mimeMessageHelper.getMimeMessage().getRecipients(Message.RecipientType.BCC)[0].toString()).isEqualTo("bcc.1@gmail.com");
    }

    @Test
    public void should_send_simple_with_attachments() throws UnsupportedEncodingException, MessagingException {
        // given
        File file = new File(this.getClass().getClassLoader().getResource("files/test.txt").getFile());
        File[] files = new File[1];
        files[0] = file;
        // when
        when(javaMailSender.createMimeMessage())
                .thenReturn(mimeMessage);
        //mailService = mock(MailServiceImpl.class);
        // then
        mailService.simpleWithAttachments(UTILITIES, true, files);
        //verify(mailService).preBuild(mimeMessage, UTILITIES, true, true);
        verify(javaMailSender).createMimeMessage();
        verify(javaMailSender).send(mimeMessage);
    }

    @Test
    public void should_send_simple_mail_with_attachments_instanciateof_inputStreamSource_array() throws IOException {
        // given
        final ByteArrayOutputStream stream = createInMemoryDocument("test document text");
        final InputStreamSource iss = new ByteArrayResource(stream.toByteArray());
        InputStreamSource[] files = new InputStreamSource[1];
        String[] filenames = new String[1];
        filenames[0] = "file.txt";
        files[0] = iss;
        // when
        when(javaMailSender.createMimeMessage())
                .thenReturn(mimeMessage);
        //mailService = mock(MailServiceImpl.class);
        // then
        mailService.simpleWithAttachments(UTILITIES, true, files, filenames);
        //verify(mailService).preBuild(mimeMessage, UTILITIES, true, true);
        verify(javaMailSender).createMimeMessage();
        verify(javaMailSender).send(mimeMessage);
    }

    @Test
    public void should_send_an_amil_with_template() {
        // Given
        Context context = new Context(Locale.FRANCE);
        // when
        when(javaMailSender.createMimeMessage())
                .thenReturn(mimeMessage);
        when(templateEngine.process(
                UTILITIES.getContent(), context))
                .thenReturn("<div>content TDD</div>");
        // then
        mailService.withTemplate(UTILITIES, new HashMap<>());
        verify(javaMailSender).createMimeMessage();
        verify(javaMailSender).send(mimeMessage);
    }

    @Test
    public void should_send_an_amil_with_template_and_attachment() {
        // Given
        Context context = new Context(Locale.FRANCE);
        // given
        File file = new File(this.getClass().getClassLoader().getResource("files/test.txt").getFile());
        File[] files = new File[1];
        files[0] = file;
        // when
        when(javaMailSender.createMimeMessage())
                .thenReturn(mimeMessage);
        when(templateEngine.process(
                UTILITIES.getContent(), context))
                .thenReturn("<div>content TDD</div>");
        // then
        mailService.withTemplateAndAttachments(UTILITIES, new HashMap<>(), files);
        verify(javaMailSender).createMimeMessage();
        verify(javaMailSender).send(mimeMessage);
    }


    private ByteArrayOutputStream createInMemoryDocument(String documentBody) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(documentBody.getBytes());
        return outputStream;
    }
}