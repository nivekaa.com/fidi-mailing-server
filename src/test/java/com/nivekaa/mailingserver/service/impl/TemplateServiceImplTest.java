package com.nivekaa.mailingserver.service.impl;

import com.nivekaa.mailingserver.model.Template;
import com.nivekaa.mailingserver.model.TemplateField;
import com.nivekaa.mailingserver.repositories.TemplateFieldRepository;
import com.nivekaa.mailingserver.repositories.TemplateRepository;
import com.nivekaa.mailingserver.service.dto.TemplateDTO;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author kevin
 * Created 10/10/2020 at 03:33
 * Class com.nivekaa.mailingserver.service.impl.TemplateServiceImplTest
 */

public class TemplateServiceImplTest {
    // @Mock
    private TemplateRepository repository;
    private TemplateFieldRepository fieldRepository;
    private TemplateServiceImpl service;
    private TemplateDTO dto;
    private Template entity;

    @Before
    public void setUp() throws Exception {
        repository = mock(TemplateRepository.class);
        fieldRepository = mock(TemplateFieldRepository.class);
        service = new TemplateServiceImpl(repository, fieldRepository);
        dto = TemplateDTO.builder()
                .code("TEMPATE-HR-0003")
                .content("lorem ipsum itjum")
                .dataModel("com.nivekaa.ms_hr.model.User")
                .entrepriseId(1L)
                .build();
        //entity = dto.toEntity();
        //entity.setId(1L);
    }

    @Test
    public void test_save() {
        // given
        Template entty = TemplateDTO.builder()
                .code("TEMPATE-HR-0003")
                .content("lorem ipsum itjum")
                .dataModel("com.nivekaa.ms_hr.model.User")
                .entrepriseId(1L)
                .id(1L)
                .build()
                .toEntity();
        // when
        when(repository.save(dto.toEntity())).thenReturn(entty);

        //System.err.println(entty);
        //System.err.println(dto.toEntity());

        // then
        //TemplateDTO response = service.save(dto);
        //assertThat(response).isNotNull();
        //assertThat(response.getEntrepriseId()).isEqualTo(1L);
    }

    @Test
    public void update() {
    }

    @Test
    public void findOne() {
    }

    @Test
    public void testFindOne() {
    }

    @Test
    public void findAll() {
    }
}