package com.nivekaa.mailingserver.service.steps;

import com.nivekaa.mailingserver.model.MailingConfiguration;
import com.nivekaa.mailingserver.repositories.MailingConfigurationRepository;
import com.nivekaa.mailingserver.service.MailingConfigurationService;
import com.nivekaa.mailingserver.service.dto.MConfigDTO;
import io.cucumber.java8.En;
import org.mockito.InjectMocks;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author nivekaa
 * Created 13/08/2020 at 23:02
 * Class com.nivekaa.mailingserver.service.steps.MailingConfigurationServiceStepDefinitions
 */

public class MailingConfigurationServiceStepDefinitions implements En {
    private MailingConfigurationRepository configurationRepository;
    @InjectMocks
    private MailingConfigurationService service;
    private Long enterpriseId;
    private MConfigDTO mConfigDTO = MConfigDTO.builder()
            .fromName("my_NAME")
            .fromEmail("my.email@test.com")
            .footerContent("DEFAULT_FOOTER")
            .headerContent("DEFAUL_HEARDER")
            .outgoingMethod("method d_envoie")
            .build();
    public MailingConfigurationServiceStepDefinitions(){
        When("^Je veux enregistrer une nouvelle configuration dont ID = (\\d+)$", (Long eId) -> {
            enterpriseId = eId;
            configurationRepository = mock(MailingConfigurationRepository.class);
            service = new MailingConfigurationService(configurationRepository);
            mConfigDTO.setEntrepriseId(enterpriseId);
            MailingConfiguration entity = mConfigDTO.toEntity();
            entity.setId(9L);
            when(configurationRepository.save(
                    mConfigDTO.toEntity()
            )).thenReturn(entity);
        });

        // voici les infos header=DEFAUL_HEARDER footer=DEFAULT_FOOTER outgoingMethod=method d'envoie fromEmail=my.email@test.com fromName=my_NAME
        Then("^voici les infos header=(.*) footer=(.*) outgoingMethod=(.*) fromEmail=(.*) fromName=(.*)$", (
                String header, String footer, String outgoing, String fromEmail, String fromName
        ) -> {
            MConfigDTO saved = service.save(
                    MConfigDTO.builder()
                            .outgoingMethod(outgoing)
                            .entrepriseId(enterpriseId)
                            .footerContent(footer)
                            .headerContent(header)
                            .fromEmail(fromEmail)
                            .fromName(fromName)
                            .build()
            );
            assertThat(saved).isNotNull();
            assertThat(saved.getId()).isEqualTo(9L);
        });

        // UPDATE CONFIGS
        When("^je veux mettre a jour ma configuration d'ID (\\d+). Entreprise ID = (\\d+)$", (Long id, Long eId) -> {
            configurationRepository = mock(MailingConfigurationRepository.class);
            service = new MailingConfigurationService(configurationRepository);
            MConfigDTO newmcg = mConfigDTO;
            newmcg.setHeaderContent("HEADER_MODIFYER");
            mConfigDTO.setEntrepriseId(enterpriseId);
            mConfigDTO.setId(id);
            when(configurationRepository.findByIdAndEntrepriseId(id, eId))
                    .thenReturn(Optional.of(mConfigDTO.toEntity()));
            when(configurationRepository.save(newmcg.toEntity()))
                    .thenReturn(newmcg.toEntity());
        });

        Then("^modifier header par (.*)$", (String h)-> {
            mConfigDTO.setHeaderContent(h);
            MConfigDTO updated = service.update(mConfigDTO);
            assertThat(updated).isNotNull();
            assertThat(updated.getHeaderContent()).isNotNull();
            assertThat(updated.getHeaderContent()).isEqualTo(h);
        });

        // Fetch config
        When("^je veux la configuration de l'entreprise d'ID (\\d+)$", (Long id) -> {
            configurationRepository = mock(MailingConfigurationRepository.class);
            service = new MailingConfigurationService(configurationRepository);
            enterpriseId = id;
            // mConfigDTO.setId(18L);
            mConfigDTO.setEntrepriseId(enterpriseId);
            MailingConfiguration entity = mConfigDTO.toEntity();
            entity.setId(18L);
            when(configurationRepository.findByEntrepriseId(enterpriseId))
                    .thenReturn(Optional.of(entity));
        });

        Then("^elle doit être non null et doit avoir avoir le fromEmail$", () -> {
            MConfigDTO fetched = service.findByEnterprise(enterpriseId);
            assertThat(fetched).isNotNull();
            assertThat(fetched.getFromEmail()).isNotNull();
            assertThat(fetched.getId()).isEqualTo(18L);
        });

        // Fetch config
        When("^je veux la configuration de l'entreprise d'ID (\\d+) _$", (Long id) -> {
            configurationRepository = mock(MailingConfigurationRepository.class);
            service = new MailingConfigurationService(configurationRepository);
            enterpriseId = id;
            // mConfigDTO.setId(18L);
            when(configurationRepository.findByEntrepriseId(enterpriseId))
                    .thenReturn(Optional.empty());
        });

        Then("^elle doit léver une exception", () -> {
            // MConfigDTO fetched = service.findByEnterprise(enterpriseId);
            catchThrowable(() -> service.findByEnterprise(enterpriseId));
        });
    }
}