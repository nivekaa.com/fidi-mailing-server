package com.nivekaa.mailingserver.service.steps;

import com.nivekaa.mailingserver.model.Mail;
import com.nivekaa.mailingserver.repositories.FileStoreRepository;
import com.nivekaa.mailingserver.repositories.MailEntityRepository;
import com.nivekaa.mailingserver.service.MailEntityService;
import com.nivekaa.mailingserver.service.dto.MailDTO;
import io.cucumber.java8.En;
import org.mockito.InjectMocks;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author nivekaa
 * Created 13/08/2020 at 13:47
 * Class com.nivekaa.mailingserver.service.steps.MailServiceStepsDefinition
 */
public class MailServiceStepDefinitions implements En {
    // CE QUI EST A TESTER
    private FileStoreRepository fileStoreRepositoryTest;
    private MailEntityRepository mailEntityRepositoryTest;
    @InjectMocks
    private MailEntityService mailService;
    private List<Mail> mailList;
    private Long enterpriseId;
    private Long mailId;
    private Mail input = null;

    public MailServiceStepDefinitions() {
        // LETCTURE MANY MAILS
        When("^je veux la liste des mails$", ()-> {
            fileStoreRepositoryTest = mock(FileStoreRepository.class);
            mailEntityRepositoryTest = mock(MailEntityRepository.class);
            mailService = new MailEntityService(mailEntityRepositoryTest, fileStoreRepositoryTest);
            Mail m1 = Mail.builder()
                    .id(1L).langKey("fr").to("to@test.com")
                    .from("from@test.com").content("test content").entrepriseId(1L)
                    .subject("BDD test").cc("cc@test.com").noFooter(false).build();
            Mail m2 = Mail.builder()
                    .id(2L).langKey("fr").to("to2@test.com")
                    .from("from2@test.com").content("test content 2").entrepriseId(1L)
                    .subject("BDD test").cc("cc2@test.com").noFooter(false).build();
            Mail m3 = Mail.builder()
                    .id(3L).langKey("en").to("to3@test.com")
                    .from("from3@test.com").content("test content 3").entrepriseId(1L)
                    .subject("BDD test").cc("cc3@test.com").noFooter(false).build();
            mailList = Arrays.asList(m1, m2, m3);
        });

        When("^l'identifiant de l'entreprise est (\\d+)$", (Long eId)-> {
            assertThat(eId).isNotNull();
            assertThat(eId).isNotZero();
            when(mailEntityRepositoryTest.findAllByEntrepriseId(eId, Pageable.unpaged()))
                    .thenReturn((new PageImpl<>(mailList)));
            enterpriseId = eId;
        });

        Then("^la liste est non vide, la liste contient (\\d+) elements, le destinataire de l'elemnent (\\d+) est (.*)$",
                (Integer count, Integer position, String _to) -> {
            List<MailDTO> result = mailService.getAllByEntreprise(enterpriseId);
            assertThat(result).isNotNull();
            assertThat(result.size()).isEqualTo(count);
            assertThat(result.get(position-1).getTo()).isEqualTo(_to);
        });

        // Fetch one
        When("^je veux un mail dont l'identifiant ID est (\\d+), l'Id de l'entreprise est (\\d+)$", (Long id, Long eid) -> {
            assertThat(id).isNotNull();
            assertThat(eid).isNotNull();
            fileStoreRepositoryTest = mock(FileStoreRepository.class);
            mailEntityRepositoryTest = mock(MailEntityRepository.class);
            mailService = new MailEntityService(mailEntityRepositoryTest, fileStoreRepositoryTest);
            Mail m1 = Mail.builder()
                    .id(1L).langKey("fr").to("to@test.com")
                    .from("from@test.com").content("test content").entrepriseId(1L)
                    .subject("BDD test").cc("cc@test.com").noFooter(false).build();
            when(mailEntityRepositoryTest.findByIdAndEntrepriseId(id, eid))
                    .thenReturn(Optional.ofNullable(m1));
            enterpriseId = eid;
            mailId = id;
        });

        Then("^l'email en question doit être not null et l'expediteur doit etre (.*) et destinateur (.*)$", (String from, String to) -> {
            MailDTO result = mailService.getByEntreprise(mailId, enterpriseId);
            assertThat(result).isNotNull();
            assertThat(result.getTo()).isEqualTo(to);
            assertThat(result.getFrom()).isEqualTo(from);
        });

        // Add new
        Given("^preparer l'enregistrement d'un mail$", () -> {
            fileStoreRepositoryTest = mock(FileStoreRepository.class);
            mailEntityRepositoryTest = mock(MailEntityRepository.class);
            mailService = new MailEntityService(mailEntityRepositoryTest, fileStoreRepositoryTest);
        });

        When("^quand je fournis les infos: to=(.*) from=(.*) subject=(.*) content=(.*)$", (
                String to, String from, String subject, String content
        ) -> {
            input = Mail.builder()
                    .to(to)
                    .from(from)
                    .subject(subject)
                    .content(content)
                    .build();
            Mail saved = input;
            saved.setId(1L);
            when(mailEntityRepositoryTest.save(input))
                    .thenReturn(saved);
        });

        Then("^Enregistrer. le resultat doit etre non null et (.*) comme object du mail nouvellement registré$", (String obj) -> {
            MailDTO result = mailService.save(input);
            assertThat(result).isNotNull();
            assertThat(result.getSubject()).isEqualTo(obj);
        });
    }
}
