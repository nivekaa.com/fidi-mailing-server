package com.nivekaa.mailingserver.service.steps;

import com.nivekaa.mailingserver.model.FileStore;
import com.nivekaa.mailingserver.model.Mail;
import com.nivekaa.mailingserver.repositories.FileStoreRepository;
import com.nivekaa.mailingserver.service.FileStoreService;
import com.nivekaa.mailingserver.service.dto.FileStoreDTO;
import io.cucumber.java8.En;
import org.mockito.InjectMocks;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author nivekaa
 * Created 14/08/2020 at 02:10
 * Class com.nivekaa.mailingserver.service.steps.FileStoreServiceStepDefinitions
 */

public class FileStoreServiceStepDefinitions implements En {
    private Long entrepriseId;
    private FileStoreRepository fileStoreRepository;
    @InjectMocks
    private FileStoreService service;
    private FileStore entity, toSave;
    public FileStoreServiceStepDefinitions() {
        When("^je veux enregistrer un nouveau fichier. Entreprise ID (\\d+)$", (Long eId) -> {
            entrepriseId = eId;
            fileStoreRepository = mock(FileStoreRepository.class);
            service = new FileStoreService(fileStoreRepository);
            Mail mail = Mail.builder().id(1L).build();
            entity = FileStore.builder()
                    .fileType("applicattion_psd")
                    .entrepriseId(entrepriseId)
                    .filename("file.psd")
                    .location("_opt_app_file")
                    .mail(mail)
                    .build();
            FileStore save = entity;
            save.setId(1L);
            when(fileStoreRepository.save(entity))
                    .thenReturn(save);
        });

        // location=/opt/app/file.psd filename=file.psd fileType=applicattion/psd mailId=1
        When("^location=(.*) filename=(.*) fileType=(.*) mailId=(\\d+)$", (
                String loc, String fname, String ftype, Long mailId
        ) -> {
            toSave = FileStore.builder()
                    .entrepriseId(entrepriseId)
                    .filename(fname)
                    .fileType(ftype)
                    .location(loc)
                    .mail(Mail.builder().id(mailId).build())
                    .build();
        });

        Then("^le resultat doit etre non null$", () -> {
            FileStoreDTO result = service.save(entity);
            assertThat(result).isNotNull();
            assertThat(result).isInstanceOf(FileStoreDTO.class);
            assertThat(result.getId()).isEqualTo(1L);
        });
    }
}