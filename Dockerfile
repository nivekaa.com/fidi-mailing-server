FROM openjdk:8-jdk-alpine

# User session
RUN addgroup -S fidi && adduser -S fidi -G fidi
#USER fidi:fidi
# Specify the project's maintainer
LABEL maintainer="kevin kemta <kevinlactiokemta@gmail.com>"

# Definer the default VM/Devise language
ENV LANG='fr_FR.UTF-8' LANGUAGE='fr_FR:fr'

# create deplyments/logs dir where all app logs will be storing using logback
RUN mkdir /logs \
    && chown fidi /logs \
    && chmod -R 777 /logs/

# Copy jar/war file that will be used
COPY target/mailing-server-*.war app.war

# Create config dir
RUN mkdir /config

# Copy the current files containing in local dir *configs*
COPY configs/ /config

# copy entry.sh file in root VM, it will use to run app
COPY entrypoint.sh /

# Specify the workspace
WORKDIR /

# The port on which the app will run
EXPOSE 8080

# Finally run the app.war
ENTRYPOINT ["sh", "/entrypoint.sh"]